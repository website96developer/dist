<?php
class ControllerApiSynchronizer extends Controller {
	public function index() {
		$this->load->model('catalog/product');
		if(!isset($_POST['key']) || $_POST['key']!='o9NSg5T9RK0LyP6M}x|lrp2r~FQc~R1Lg7V9DnntLd#Qjl4MVq~xDFNIDd9DEC22'){


			$data['breadcrumbs'][] = array(
				'text' => '',
				'href' => $this->url->link('/')
			);
			
			$this->document->setTitle($this->language->get('text_error'));

			$data['heading_title'] = $this->language->get('text_error');

			$data['text_error'] = $this->language->get('text_error');

			$data['button_continue'] = $this->language->get('button_continue');

			$data['continue'] = $this->url->link('common/home');

			$this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . ' 404 Not Found');

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			$this->response->setOutput($this->load->view('error/not_found', $data));
		}
		else{
			foreach($_POST['products'] as $product)
				$this->model_catalog_product->synchronizeProduct($product);
			$this->response->setOutput(json_encode($_POST['products']));
		}
	}
}