<?php

class ControllerInformationServicesInfo extends Controller {
    public function info(){
		$this->language->load('information/services');

		$this->load->model('catalog/services');

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'href' => $this->url->link('common/home'),
			'text' => $this->language->get('text_home')
		);

		$data['breadcrumbs'][] = array(
			'href' => $this->url->link('information/services'),
			'text' => $this->language->get('heading_title')
		);

		if (isset($this->request->get['services_id'])) {
			$services_id = $this->request->get['services_id'];
		} else {
			$services_id = 0;
		}

		$services_info = $this->model_catalog_services->getServicesStory($this->request->get['services_id']);

		if ($services_info) {

			$this->document->addScript('catalog/view/javascript/jquery/magnific/jquery.magnific-popup.min.js');
			$this->document->addStyle('catalog/view/javascript/jquery/magnific/magnific-popup.css');

			if ($services_info['meta_title']) {
				$this->document->setTitle($services_info['meta_title']);
			} else {
				$this->document->setTitle($services_info['title']);
			}

			$this->document->setDescription($services_info['meta_description']);
			$this->document->setKeywords($services_info['meta_keyword']);

			if ($services_info['meta_h1']) {
				$data['heading_title'] = $services_info['meta_h1'];
			} else {
				$data['heading_title'] = $services_info['title'];
			}

			$data['breadcrumbs'][] = array(
				'text' => $services_info['title'],
				'href' => $this->url->link('information/services/info', 'services_id=' . $services_id)
			);

			$this->document->addLink($this->url->link('information/services', 'services_id=' . $this->request->get['services_id']),
				'canonical');

			$data['description'] = html_entity_decode($services_info['description']);

			$data['viewed'] = $services_info['viewed'];
			$data['posted'] = date($this->language->get('date_format_short'), strtotime($services_info['date_added']));

			if ($this->config->get('services_setting')) {
				$services_setting = $this->config->get('services_setting');
			}else{
				$services_setting['services_thumb_width']  = '220';
				$services_setting['services_thumb_height'] = '220';
				$services_setting['services_popup_width']  = '560';
				$services_setting['services_popup_height'] = '560';
			}

			if(isset($services_setting['services_share'])){
				$data['services_share'] = $services_setting['services_share'];
			}else{
				$data['services_share'] = false;
			}

			$this->load->model('tool/image');
            
            $data['images'] = array();

			$results = $this->model_catalog_services->getServicesImages($this->request->get['services_id']);

			foreach ($results as $result) {
				$data['images'][] = array(
					'popup' => $this->model_tool_image->noresize($result['image'], $this->config->get($this->config->get('config_theme') . '_image_popup_width'), $this->config->get($this->config->get('config_theme') . '_image_popup_height')),
					'thumb' => $this->model_tool_image->resize($result['image'], $this->config->get($this->config->get('config_theme') . '_image_additional_width'), $this->config->get($this->config->get('config_theme') . '_image_additional_height'))
				);
			}

			if ($services_info['image']) {
				$data['image'] = true;
			} else {
				$data['image'] = false;
			}
			if($services_info['image']){
				$data['thumb'] = $this->model_tool_image->resize($services_info['image'], $services_setting['services_thumb_width'],
				$services_setting['services_thumb_height']);
				$data['popup'] = $this->model_tool_image->noresize($services_info['image'], $services_setting['services_popup_width'],
				$services_setting['services_popup_height']);
			}else{
				$data['thumb'] = false;
				$data['popup'] = false;
			}

			$data['button_services'] = $this->language->get('button_services');
			$data['button_continue'] = $this->language->get('button_continue');

			$data['services_list'] = $this->url->link('information/services');
			$data['continue'] = $this->url->link('common/home');

			if (isset($_SERVER['HTTP_REFERER'])) {
				$data['referred'] = $_SERVER['HTTP_REFERER'];
			}

			$data['refreshed'] = 'http://' . $_SERVER['HTTP_HOST'] . '' . $_SERVER['REQUEST_URI'];

			if (isset($data['referred'])) {
				$this->model_catalog_services->updateViewed($this->request->get['services_id']);
			}

			$data['description'] = html_entity_decode($services_info['description'], ENT_QUOTES, 'UTF-8');

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			$this->response->setOutput($this->load->view('information/services', $data));

		} else {
			$url = '';

			if (isset($this->request->get['services_id'])) {
				$url .= '&services_id=' . $this->request->get['services_id'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_error'),
				'href' => $this->url->link('information/services/info', $url)
			);

			$this->document->setTitle($this->language->get('text_error'));

			$data['heading_title'] = $this->language->get('text_error');

			$data['text_error'] = $this->language->get('text_error');

			$data['button_continue'] = $this->language->get('button_continue');

			$data['continue'] = $this->url->link('common/home');

			$this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . ' 404 Not Found');

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			$this->response->setOutput($this->load->view('error/not_found', $data));
		}
	}
}