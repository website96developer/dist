<?php

class ControllerInformationAbout extends Controller {

	public function index(){
		$this->load->language('information/about');

		$this->load->model('catalog/about');

		$this->load->model('tool/image');

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'n.date_added';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'DESC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		if (isset($this->request->get['limit'])) {
			$limit = (int)$this->request->get['limit'];
		} else {
			$limit = $limit = $this->config->get($this->config->get('config_theme') . '_product_limit');
		}

		$this->document->setTitle($this->language->get('heading_title'));

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_empty'] = $this->language->get('text_empty');

		$data['button_continue'] = $this->language->get('button_continue');
		$data['continue'] = $this->url->link('common/home');

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if (isset($this->request->get['limit'])) {
			$url .= '&limit=' . $this->request->get['limit'];
		}

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('information/about', $url)
		);

		$filter_data = array(
			'sort' => $sort,
			'order' => $order,
			'start' => ($page - 1) * $limit,
			'limit' => $limit
		);

		$about_total = $this->model_catalog_about->getTotalAbout();
		$about_list = $this->model_catalog_about->getAbout($filter_data);

		$data['about_list'] = array();
		
		if ($about_list) {

			$this->document->setTitle($this->language->get('heading_title'));

			$data['heading_title'] = $this->language->get('heading_title');
			$data['text_empty'] = $this->language->get('text_empty');

			$data['button_grid'] = $this->language->get('button_grid');
			$data['button_list'] = $this->language->get('button_list');

            /*mmr*/
            $data['moneymaker2_catalog_default_view'] = $this->config->get('moneymaker2_catalog_layout_default');
            $data['moneymaker2_catalog_layout_switcher_hide'] = $this->config->get('moneymaker2_catalog_layout_switcher_hide');
            /*mmr*/

			$data['text_sort'] = $this->language->get('text_sort');
			$data['text_limit'] = $this->language->get('text_limit');

			$data['text_more'] = $this->language->get('text_more');

			$about_setting = array();

			if ($this->config->get('about_setting')) {
				$about_setting = $this->config->get('about_setting');
			}else{
				$about_setting['description_limit'] = '300';
				$about_setting['about_thumb_width'] = '220';
				$about_setting['about_thumb_height'] = '220';
			}

			foreach ($about_list as $result) {

				if($result['image']){
					$image = $this->model_tool_image->resize($result['image'], $about_setting['about_thumb_width'], $about_setting['about_thumb_height']);
				}else{
					$image = false;
				}

				$data['about_list'][] = array(
					'title' => $result['title'],
					'thumb' => $image,
					'viewed' => $result['viewed'],
					'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES,
						'UTF-8')), 0, $about_setting['description_limit']),
					'href' => $this->url->link('information/about/info', 'about_id=' . $result['about_id']),
					'posted' => date($this->language->get('date_format_short'), strtotime($result['date_added']))
				);
			}

		}

		$url = '';

		if (isset($this->request->get['limit'])) {
			$url .= '&limit=' . $this->request->get['limit'];
		}

		$data['sorts'] = array();

		$data['sorts'][] = array(
			'text' => $this->language->get('text_title_asc'),
			'value' => 'nd.title-ASC',
			'href' => $this->url->link('information/about', 'sort=nd.title&order=ASC' . $url)
		);

		$data['sorts'][] = array(
			'text' => $this->language->get('text_title_desc'),
			'value' => 'nd.title-DESC',
			'href' => $this->url->link('information/about', 'sort=nd.title&order=DESC' . $url)
		);

		$data['sorts'][] = array(
			'text' => $this->language->get('text_date_asc'),
			'value' => 'n.date_added-ASC',
			'href' => $this->url->link('information/about', 'sort=n.date_added&order=ASC' . $url)
		);

		$data['sorts'][] = array(
			'text' => $this->language->get('text_date_desc'),
			'value' => 'n.date_added-DESC',
			'href' => $this->url->link('information/about', 'sort=n.date_added&order=DESC' . $url)
		);		

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		
		$data['limits'] = array();

		$limits = array_unique(array($this->config->get($this->config->get('config_theme') . '_product_limit'), 25, 50, 75, 100));

		sort($limits);

		foreach ($limits as $value) {
			$data['limits'][] = array(
				'text' => $value,
				'value' => $value,
				'href' => $this->url->link('information/about', $url . '&limit=' . $value)
			);
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['limit'])) {
			$url .= '&limit=' . $this->request->get['limit'];
		}		

		$pagination = new Pagination();
		$pagination->total = $about_total;
		$pagination->page = $page;
		$pagination->limit = $limit;
		$pagination->url = $this->url->link('information/about', $url . '&page={page}');

		$data['pagination'] = $pagination->render();
		$data['results'] = sprintf($this->language->get('text_pagination'), ($about_total) ? (($page - 1) * $limit) + 1 : 0, ((($page - 1) * $limit) > ($about_total - $limit)) ? $about_total : ((($page - 1) * $limit) + $limit), $about_total, ceil($about_total / $limit));

		// http://googlewebmastercentral.blogspot.com/2011/09/pagination-with-relnext-and-relprev.html
		if ($page == 1) {
			$this->document->addLink($this->url->link('information/about', '', true), 'canonical');
		} elseif ($page == 2) {
			$this->document->addLink($this->url->link('information/about', '', true), 'prev');
		} else {
			$this->document->addLink($this->url->link('information/about', '&page=' . ($page - 1), true), 'prev');
		}

		if ($limit && ceil($about_total / $limit) > $page) {
			$this->document->addLink($this->url->link('information/about', '&page=' . ($page + 1), true), 'next');
		}

		$data['sort'] = $sort;
		$data['order'] = $order;
		$data['limit'] = $limit;

		$data['continue'] = $this->url->link('common/home');

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$this->response->setOutput($this->load->view('information/about_list', $data));
	}

	public function info(){
		$this->language->load('information/about');

		$this->load->model('catalog/about');

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'href' => $this->url->link('common/home'),
			'text' => $this->language->get('text_home')
		);

		$data['breadcrumbs'][] = array(
			'href' => $this->url->link('information/about'),
			'text' => $this->language->get('heading_title')
		);

		if (isset($this->request->get['about_id'])) {
			$about_id = $this->request->get['about_id'];
		} else {
			$about_id = 0;
		}

		$about_info = $this->model_catalog_about->getAboutStory($this->request->get['about_id']);

		if ($about_info) {

			$this->document->addScript('catalog/view/javascript/jquery/magnific/jquery.magnific-popup.min.js');
			$this->document->addStyle('catalog/view/javascript/jquery/magnific/magnific-popup.css');

			if ($about_info['meta_title']) {
				$this->document->setTitle($about_info['meta_title']);
			} else {
				$this->document->setTitle($about_info['title']);
			}

			$this->document->setDescription($about_info['meta_description']);
			$this->document->setKeywords($about_info['meta_keyword']);

			if ($about_info['meta_h1']) {
				$data['heading_title'] = $about_info['meta_h1'];
			} else {
				$data['heading_title'] = $about_info['title'];
			}

			$data['breadcrumbs'][] = array(
				'text' => $about_info['title'],
				'href' => $this->url->link('information/about/info', 'about_id=' . $about_id)
			);

			$this->document->addLink($this->url->link('information/about', 'about_id=' . $this->request->get['about_id']),
				'canonical');

			$data['description'] = html_entity_decode($about_info['description']);

			$data['viewed'] = $about_info['viewed'];
			$data['posted'] = date($this->language->get('date_format_short'), strtotime($about_info['date_added']));

			if ($this->config->get('about_setting')) {
				$about_setting = $this->config->get('about_setting');
			}else{
				$about_setting['about_thumb_width']  = '220';
				$about_setting['about_thumb_height'] = '220';
				$about_setting['about_popup_width']  = '560';
				$about_setting['about_popup_height'] = '560';
			}

			if(isset($about_setting['about_share'])){
				$data['about_share'] = $about_setting['about_share'];
			}else{
				$data['about_share'] = false;
			}

			$this->load->model('tool/image');
            
            $data['images'] = array();

			$results = $this->model_catalog_about->getAboutImages($this->request->get['about_id']);

			foreach ($results as $result) {
				$data['images'][] = array(
					'popup' => $this->model_tool_image->noresize($result['image'], $this->config->get($this->config->get('config_theme') . '_image_popup_width'), $this->config->get($this->config->get('config_theme') . '_image_popup_height')),
					'thumb' => $this->model_tool_image->resize($result['image'], $this->config->get($this->config->get('config_theme') . '_image_additional_width'), $this->config->get($this->config->get('config_theme') . '_image_additional_height'))
				);
			}

			if ($about_info['image']) {
				$data['image'] = true;
			} else {
				$data['image'] = false;
			}
			if($about_info['image']){
				$data['thumb'] = $this->model_tool_image->resize($about_info['image'], $about_setting['about_thumb_width'],
				$about_setting['about_thumb_height']);
				$data['popup'] = $this->model_tool_image->noresize($about_info['image'], $about_setting['about_popup_width'],
				$about_setting['about_popup_height']);
			}else{
				$data['thumb'] = false;
				$data['popup'] = false;
			}

			$data['button_about'] = $this->language->get('button_about');
			$data['button_continue'] = $this->language->get('button_continue');

			$data['about_list'] = $this->url->link('information/about');
			$data['continue'] = $this->url->link('common/home');

			if (isset($_SERVER['HTTP_REFERER'])) {
				$data['referred'] = $_SERVER['HTTP_REFERER'];
			}

			$data['refreshed'] = 'http://' . $_SERVER['HTTP_HOST'] . '' . $_SERVER['REQUEST_URI'];

			if (isset($data['referred'])) {
				$this->model_catalog_about->updateViewed($this->request->get['about_id']);
			}

			$data['description'] = html_entity_decode($about_info['description'], ENT_QUOTES, 'UTF-8');

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			$this->response->setOutput($this->load->view('information/about', $data));

		} else {
			$url = '';

			if (isset($this->request->get['about_id'])) {
				$url .= '&about_id=' . $this->request->get['about_id'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_error'),
				'href' => $this->url->link('information/about/info', $url)
			);

			$this->document->setTitle($this->language->get('text_error'));

			$data['heading_title'] = $this->language->get('text_error');

			$data['text_error'] = $this->language->get('text_error');

			$data['button_continue'] = $this->language->get('button_continue');

			$data['continue'] = $this->url->link('common/home');

			$this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . ' 404 Not Found');

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			$this->response->setOutput($this->load->view('error/not_found', $data));
		}
	}
}