<?php

class ControllerInformationEventsInfo extends Controller {
	public function info(){
		$this->language->load('information/events');

		$this->load->model('catalog/events');

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'href' => $this->url->link('common/home'),
			'text' => $this->language->get('text_home')
		);

		$data['breadcrumbs'][] = array(
			'href' => $this->url->link('information/events'),
			'text' => $this->language->get('heading_title')
		);

		if (isset($this->request->get['events_id'])) {
			$events_id = $this->request->get['events_id'];
		} else {
			$events_id = 0;
		}

		$events_info = $this->model_catalog_events->getEventsStory($this->request->get['events_id']);

		if ($events_info) {

			$this->document->addScript('catalog/view/javascript/jquery/magnific/jquery.magnific-popup.min.js');
			$this->document->addStyle('catalog/view/javascript/jquery/magnific/magnific-popup.css');

			if ($events_info['meta_title']) {
				$this->document->setTitle($events_info['meta_title']);
			} else {
				$this->document->setTitle($events_info['title']);
			}

			$this->document->setDescription($events_info['meta_description']);
			$this->document->setKeywords($events_info['meta_keyword']);

			if ($events_info['meta_h1']) {
				$data['heading_title'] = $events_info['meta_h1'];
			} else {
				$data['heading_title'] = $events_info['title'];
			}

			$data['breadcrumbs'][] = array(
				'text' => $events_info['title'],
				'href' => $this->url->link('information/events/info', 'events_id=' . $events_id)
			);

			$this->document->addLink($this->url->link('information/events', 'events_id=' . $this->request->get['events_id']),
				'canonical');

			$data['description'] = html_entity_decode($events_info['description']);

			$data['viewed'] = $events_info['viewed'];
			$data['posted'] = date($this->language->get('date_format_short'), strtotime($events_info['date_added']));

			if ($this->config->get('events_setting')) {
				$events_setting = $this->config->get('events_setting');
			}else{
				$events_setting['events_thumb_width']  = '220';
				$events_setting['events_thumb_height'] = '220';
				$events_setting['events_popup_width']  = '560';
				$events_setting['events_popup_height'] = '560';
			}

			if(isset($events_setting['events_share'])){
				$data['events_share'] = $events_setting['events_share'];
			}else{
				$data['events_share'] = false;
			}

			$this->load->model('tool/image');

			if ($events_info['image']) {
				$data['image'] = true;
			} else {
				$data['image'] = false;
			}
			if($events_info['image']){
				$data['thumb'] = $this->model_tool_image->resize($events_info['image'], $events_setting['events_thumb_width'],
				$events_setting['events_thumb_height']);
				$data['popup'] = $this->model_tool_image->noresize($events_info['image'], $events_setting['events_popup_width'],
				$events_setting['events_popup_height']);
			}else{
				$data['thumb'] = false;
				$data['popup'] = false;
			}

			$data['button_events'] = $this->language->get('button_events');
			$data['button_continue'] = $this->language->get('button_continue');

			$data['events_list'] = $this->url->link('information/events');
			$data['continue'] = $this->url->link('common/home');

			if (isset($_SERVER['HTTP_REFERER'])) {
				$data['referred'] = $_SERVER['HTTP_REFERER'];
			}

			$data['refreshed'] = 'http://' . $_SERVER['HTTP_HOST'] . '' . $_SERVER['REQUEST_URI'];

			if (isset($data['referred'])) {
				$this->model_catalog_events->updateViewed($this->request->get['events_id']);
			}

			$data['description'] = html_entity_decode($events_info['description'], ENT_QUOTES, 'UTF-8');

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			$this->response->setOutput($this->load->view('information/events', $data));

		} else {
			$url = '';

			if (isset($this->request->get['events_id'])) {
				$url .= '&events_id=' . $this->request->get['events_id'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_error'),
				'href' => $this->url->link('information/events/info', $url)
			);

			$this->document->setTitle($this->language->get('text_error'));

			$data['heading_title'] = $this->language->get('text_error');

			$data['text_error'] = $this->language->get('text_error');

			$data['button_continue'] = $this->language->get('button_continue');

			$data['continue'] = $this->url->link('common/home');

			$this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . ' 404 Not Found');

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			$this->response->setOutput($this->load->view('error/not_found', $data));
		}
	}
}