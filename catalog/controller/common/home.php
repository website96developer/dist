<?php
class ControllerCommonHome extends Controller {
	public function index() {
		$this->document->setTitle($this->config->get('config_meta_title'));
		$this->document->setDescription($this->config->get('config_meta_description'));
		$this->document->setKeywords($this->config->get('config_meta_keyword'));
        $this->load->language('information/news');

		$this->load->model('catalog/news');

		$this->load->model('tool/image');
        
        if (isset($this->request->get['limit'])) {
			$limit = (int)$this->request->get['limit'];
		} else {
			$limit = $this->config->get($this->config->get('config_theme') . '_product_limit');
		}
        
        $filter_data = array(
			'sort' => 'n.date_added',
			'order' => 'DESC',
			'start' => (1 - 1) * 3,
			'limit' => 4
		);
        
        $news_total = $this->model_catalog_news->getTotalNews();
		$news_list = $this->model_catalog_news->getNews($filter_data);
        
        $this->document->addScript('catalog/view/javascript/jquery/magnific/jquery.magnific-popup.min.js');
        $this->document->addStyle('catalog/view/javascript/jquery/magnific/magnific-popup.css');

		$data['news_list'] = array();
		
		if ($news_list) {


			$data['heading_title'] = $this->language->get('heading_title');
			$data['text_empty'] = $this->language->get('text_empty');

			$data['button_grid'] = $this->language->get('button_grid');
			$data['button_list'] = $this->language->get('button_list');

            /*mmr*/
            $data['moneymaker2_catalog_default_view'] = $this->config->get('moneymaker2_catalog_layout_default');
            $data['moneymaker2_catalog_layout_switcher_hide'] = $this->config->get('moneymaker2_catalog_layout_switcher_hide');
            /*mmr*/

			$data['text_sort'] = $this->language->get('text_sort');
			$data['text_limit'] = $this->language->get('text_limit');

			$data['text_more'] = $this->language->get('text_more');

			$news_setting = array();

			if ($this->config->get('news_setting')) {
				$news_setting = $this->config->get('news_setting');
			}else{
				$news_setting['description_limit'] = '300';
				$news_setting['news_thumb_width'] = '220';
				$news_setting['news_thumb_height'] = '220';
			}

			foreach ($news_list as $result) {

				if($result['image']){
					$image = $this->model_tool_image->resize($result['image'], $news_setting['news_thumb_width'], $news_setting['news_thumb_height']);
				}else{
					$image = false;
				}

				$data['news_list'][] = array(
					'title' => $result['title'],
					'thumb' => $image,
					'viewed' => sprintf($this->language->get('text_viewed'), $result['viewed']),
					'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES,
						'UTF-8')), 0, $news_setting['description_limit']),
					'href' => $this->url->link('information/news_info/info', 'news_id=' . $result['news_id']),
					'posted' => date($this->language->get('date_format_short'), strtotime($result['date_added']))
				);
			}

		}
        
        $this->load->model('catalog/events');
        
        $results = $this->model_catalog_events->getEventsShortsHome("4");
	
		foreach ($results as $result) {
            if ($result['image']) {
                $image = $this->model_tool_image->resize($result['image'], "150", "150");
            } else {
                $image = false;
            }
			
			$data['events'][] = array(
				'title'        		=> $result['title'],
				'thumb' 			=> $image,
				'href'         		=> $this->url->link('information/events_info/info', 'events_id=' . $result['events_id']),
				'date'   			=> date($this->language->get('date_format_short'), strtotime($result['date']))
			);
		}    
        

		if (isset($this->request->get['route'])) {
			$this->document->addLink($this->config->get('config_url'), 'canonical');
		}

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$this->response->setOutput($this->load->view('common/home', $data));
	}
}

