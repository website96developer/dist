<?php

class ModelCatalogEvents extends Model { 

	public function updateViewed($events_id) {
		$this->db->query("UPDATE " . DB_PREFIX . "events SET viewed = (viewed + 1) WHERE events_id = '" . (int)$events_id . "'");
	}

	public function getEventsStory($events_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "events n LEFT JOIN " . DB_PREFIX . "events_description nd ON (n.events_id = nd.events_id) LEFT JOIN " . DB_PREFIX . "events_to_store n2s ON (n.events_id = n2s.events_id) WHERE n.events_id = '" . (int)$events_id . "' AND nd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND n2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND n.status = '1'");
	
		return $query->row;
	}

	public function getEvents($data) {
				$sql = "SELECT * FROM " . DB_PREFIX . "events n LEFT JOIN " . DB_PREFIX . "events_description nd ON (n.events_id = nd.events_id) LEFT JOIN " . DB_PREFIX . "events_to_store n2s ON (n.events_id = n2s.events_id) WHERE nd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND n2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND n.status = '1'";

                if (!empty($data['date1'])) {
                    $sql .= " AND n.date >= '" . $data['date1'] . "'";
                }

                if (!empty($data['date2'])) {
                    $sql .= " AND n.date <= '" . $data['date2'] . "'";
                }
                
				$sort_data = array(
					'nd.title',
					'n.date_added'
				);

				if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
					$sql .= " ORDER BY " . $data['sort'];
				} else {
					$sql .= " ORDER BY n.date";
				}

				if (isset($data['order']) && ($data['order'] == 'DESC')) {
					$sql .= " DESC";
				} else {
					$sql .= " ASC";
				}
	
				if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
				$data['start'] = 0;
				}		
				if ($data['limit'] < 1) {
				$data['limit'] = 10;
				}
		
				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
				}	
		
				$query = $this->db->query($sql);
	
				return $query->rows;
				}

	public function getEventsShorts($limit) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "events n LEFT JOIN " . DB_PREFIX . "events_description nd ON (n.events_id = nd.events_id) LEFT JOIN " . DB_PREFIX . "events_to_store n2s ON (n.events_id = n2s.events_id) WHERE nd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND n2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND n.status = '1' ORDER BY n.date_added DESC LIMIT " . (int)$limit); 
	
		return $query->rows;
	}
    
    public function getEventsShortsHome($limit) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "events n LEFT JOIN " . DB_PREFIX . "events_description nd ON (n.events_id = nd.events_id) LEFT JOIN " . DB_PREFIX . "events_to_store n2s ON (n.events_id = n2s.events_id) WHERE nd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND n2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND n.status = '1' ORDER BY n.date_added DESC LIMIT " . (int)$limit); 
	
		return $query->rows;
	}

	public function getTotalEvents() {
     	$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "events n LEFT JOIN " . DB_PREFIX . "events_to_store n2s ON (n.events_id = n2s.events_id) WHERE n2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND n.status = '1'");
	
		if ($query->row) {
			return $query->row['total'];
		} else {
			return FALSE;
		}
	}	
}
?>
