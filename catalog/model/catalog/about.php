<?php

class ModelCatalogAbout extends Model { 

	public function updateViewed($about_id) {
		$this->db->query("UPDATE " . DB_PREFIX . "about SET viewed = (viewed + 1) WHERE about_id = '" . (int)$about_id . "'");
	}

	public function getAboutStory($about_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "about n LEFT JOIN " . DB_PREFIX . "about_description nd ON (n.about_id = nd.about_id) LEFT JOIN " . DB_PREFIX . "about_to_store n2s ON (n.about_id = n2s.about_id) WHERE n.about_id = '" . (int)$about_id . "' AND nd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND n2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND n.status = '1'");
	
		return $query->row;
	}
    
    public function getAboutImages($about_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "about_image WHERE about_id = '" . (int)$about_id . "' ORDER BY sort_order ASC");

		return $query->rows;
	}

	public function getAbout($data) {
				$sql = "SELECT * FROM " . DB_PREFIX . "about n LEFT JOIN " . DB_PREFIX . "about_description nd ON (n.about_id = nd.about_id) LEFT JOIN " . DB_PREFIX . "about_to_store n2s ON (n.about_id = n2s.about_id) WHERE nd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND n2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND n.status = '1'";

				$sort_data = array(
					'nd.title',
					'n.date_added'
				);

				if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
					$sql .= " ORDER BY " . $data['sort'];
				} else {
					$sql .= " ORDER BY nd.title";
				}

				if (isset($data['order']) && ($data['order'] == 'DESC')) {
					$sql .= " DESC";
				} else {
					$sql .= " ASC";
				}
	
				if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
				$data['start'] = 0;
				}		
				if ($data['limit'] < 1) {
				$data['limit'] = 10;
				}	
		
				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
				}	
		
				$query = $this->db->query($sql);
	
				return $query->rows;
				}

	public function getervicesShorts($limit) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "about n LEFT JOIN " . DB_PREFIX . "about_description nd ON (n.about_id = nd.about_id) LEFT JOIN " . DB_PREFIX . "about_to_store n2s ON (n.about_id = n2s.about_id) WHERE nd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND n2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND n.status = '1' ORDER BY n.date_added DESC LIMIT " . (int)$limit); 
	
		return $query->rows;
	}

	public function getTotalAbout() {
     	$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "about n LEFT JOIN " . DB_PREFIX . "about_to_store n2s ON (n.about_id = n2s.about_id) WHERE n2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND n.status = '1'");
	
		if ($query->row) {
			return $query->row['total'];
		} else {
			return FALSE;
		}
	}	
}
?>
