<?php

class ModelCatalogServices extends Model { 

	public function updateViewed($services_id) {
		$this->db->query("UPDATE " . DB_PREFIX . "services SET viewed = (viewed + 1) WHERE services_id = '" . (int)$services_id . "'");
	}

	public function getServicesStory($services_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "services n LEFT JOIN " . DB_PREFIX . "services_description nd ON (n.services_id = nd.services_id) LEFT JOIN " . DB_PREFIX . "services_to_store n2s ON (n.services_id = n2s.services_id) WHERE n.services_id = '" . (int)$services_id . "' AND nd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND n2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND n.status = '1'");
	
		return $query->row;
	}
    
    public function getServicesImages($services_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "services_image WHERE services_id = '" . (int)$services_id . "' ORDER BY sort_order ASC");

		return $query->rows;
	}

	public function getServices($data) {
				$sql = "SELECT * FROM " . DB_PREFIX . "services n LEFT JOIN " . DB_PREFIX . "services_description nd ON (n.services_id = nd.services_id) LEFT JOIN " . DB_PREFIX . "services_to_store n2s ON (n.services_id = n2s.services_id) WHERE nd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND n2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND n.status = '1'";

				$sort_data = array(
					'nd.title',
					'n.date_added'
				);

				if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
					$sql .= " ORDER BY " . $data['sort'];
				} else {
					$sql .= " ORDER BY nd.title";
				}

				if (isset($data['order']) && ($data['order'] == 'DESC')) {
					$sql .= " DESC";
				} else {
					$sql .= " ASC";
				}
	
				if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
				$data['start'] = 0;
				}		
				if ($data['limit'] < 1) {
				$data['limit'] = 10;
				}	
		
				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
				}	
		
				$query = $this->db->query($sql);
	
				return $query->rows;
				}

	public function getervicesShorts($limit) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "services n LEFT JOIN " . DB_PREFIX . "services_description nd ON (n.services_id = nd.services_id) LEFT JOIN " . DB_PREFIX . "services_to_store n2s ON (n.services_id = n2s.services_id) WHERE nd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND n2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND n.status = '1' ORDER BY n.date_added DESC LIMIT " . (int)$limit); 
	
		return $query->rows;
	}

	public function getTotalServices() {
     	$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "services n LEFT JOIN " . DB_PREFIX . "services_to_store n2s ON (n.services_id = n2s.services_id) WHERE n2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND n.status = '1'");
	
		if ($query->row) {
			return $query->row['total'];
		} else {
			return FALSE;
		}
	}	
}
?>
