<div class="left_block_recommend">
    <div class="left_column_title">
        Рекомендуем
    </div>
    <ul class="list-unstyled recommend_list">
        <li>
            <div class="btn_circle_border">
                <div class="recommend_list_img surprize"></div>
            </div>
            <a href="#">Подарки к праздникам</a>
        </li>
        <li>
            <div class="btn_circle_border">
                <div class="recommend_list_img pen"></div>
            </div>
            <a href="#" >Огромный ассортимент канцтоваров</a>
        </li> 
    </ul> 
</div>