<?php if($show_title) { ?>
<h3><?php echo $show_icon ? '<i class="fa fa-newspaper-o fa-3x"></i>&nbsp;' : ''; ?><?php echo $heading_title; ?></h3>
<?php } ?>
<div class="news_left_block left_block_master">
    <div class="left_column_title">
        Мастер-классы
    </div>
	<ul class="list-unstyled master_list">
    <?php foreach ($events as $events_item) { ?>
        <li class="clerfix">
            <a href="<?php echo $events_item['href']; ?>">
                <?php if($events_item['thumb']) { ?>
                <div class="master_list_pic" style="background-image: url('<?php echo $events_item['thumb']; ?>');">
                </div>
                <?php }?>
                <span><?php echo $events_item['title']; ?></span>
            </a>
        </li> 
    <?php } ?>
    </ul>
</div>