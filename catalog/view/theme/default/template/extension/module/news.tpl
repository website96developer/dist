<?php if($show_title) { ?>
<h3><?php echo $show_icon ? '<i class="fa fa-newspaper-o fa-3x"></i>&nbsp;' : ''; ?><?php echo $heading_title; ?></h3>
<?php } ?>
<div class="news_left_block left_block_master">
    <div class="left_column_title">
        Новости
    </div>
	<ul class="list-unstyled news_block_list">
    <?php foreach ($news as $news_item) { ?>
        <li>
            <a href="<?php echo $news_item['href']; ?>">
                <?php if($news_item['thumb']) { ?>
                <span class="news_block_pic">
                    <img src="<?php echo $news_item['thumb']; ?>" alt="<?php echo $news_item['title']; ?>" title="<?php echo $news_item['title']; ?>">
                </span>
                <?php }?>
                <div class="news_block_list_desc">
                    <div class="news_block_list_date">
                        <i class="fa fa-clock-o" aria-hidden="true"></i>
                        <span><?php echo $news_item['posted']; ?></span>
                    </div>
                    <div class="news_block_list_title">
                        <?php echo $news_item['title']; ?>
                    </div>
                      <div class="news_block_list_text">
                        <?php echo $news_item['description']; ?>
                    </div>
                </div>
            </a>
        </li> 
    <?php } ?>
    </ul>
</div>