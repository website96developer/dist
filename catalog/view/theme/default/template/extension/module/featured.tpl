<div class="product_carousel_wrapper">
        <div class="product_carousel_head">
            <div class="row">
                <div class="col-md-12">
                    <span class="product_carousel_title pull-left"><?php echo $heading_title; ?></span>
                    <div class="button_carousel pull-right">
                        <div id="carousel_prev_<?php echo $module_id; ?>" class="button_carousel btn_circle_border"></div>
                        <div id="carousel_next_<?php echo $module_id; ?>" class="button_carousel btn_circle_border"></div>
                    </div>
                </div>
            </div>    
        </div>
        <div class="row">
            <div class="col-md-12">
                <ul id="module_<?php echo $module_id; ?>" class="product_carousel product_carousel_4 clearfix">
                  <?php foreach ($products as $product) { ?>
                  <li class="product-layout list-unstyled">
                        <div class="product-thumb transition">
                             <div class="image">
                                <a href="<?php echo $product['href']; ?>">
                                     <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" />
                                </a>
                            </div>
                         <div class="caption">
                                <span class="product_title"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></span>
                                <ul class="product_attributes_list list-unstyled">
                                <?php foreach($product['attributes'] as $group_attr) {
                                    if($group_attr['attribute_group_id']==7) {
                                        foreach($group_attr['attribute'] as $attr) {
                                            if($attr['attribute_id']==12||$attr['attribute_id']==17) {
                                                ?>
                                                <li class="product_attributes_item">
                                                    <span class="product_caption_title"><?php echo $attr['name']; ?></span>
                                                    <span><?php echo $attr['text']; ?></span>
                                                </li>
                                                <?php
                                            }
                                        }
                                    }
                                    ?>
                                <?php } ?>
                                </ul>
                                <div class="product_caption_footer">
                                    <span class="product_count">
                                    <?php if ($product['price']) { ?>
                                          <?php if (!$product['special']) { ?>
                                          <?php echo $product['price']; ?>
                                          <i class="fa fa-rub" aria-hidden="true"></i>
                                          <?php } else { ?>
                                          <span class="price-new"><?php echo $product['special']; ?><i class="fa fa-rub" aria-hidden="true"></i></span><span class="price-old"><?php echo $product['price']; ?><i class="fa fa-rub" aria-hidden="true"></i></span>
                                          <?php } ?>
                                          <?php if ($product['tax']) { ?>
                                          <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?><i class="fa fa-rub" aria-hidden="true"></i></span>
                                          <?php } ?>
                                    <?php } ?>
                                    </span>
                                    <div class="product_caption_buy product_caption_buy__featured">
                                       <button onclick="cart.add('<?php echo $product['product_id']; ?>');" class="btn">В корзину</button>
                                       <button onclick="wishlist.add('<?php echo $product['product_id']; ?>');" class="btn_circle_background_small btn_wishlist"><i class="fa fa-star-o" aria-hidden="true"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <?php } ?>
                </ul>    
            </div>
        </div>
</div>
<script>
    $(document).ready(function(){
        $('#module_<?php echo $module_id; ?>').bxSlider({
            pager: false,
            nextSelector: '#carousel_next_<?php echo $module_id; ?>',
            prevSelector: '#carousel_prev_<?php echo $module_id; ?>',
            nextText: '<div class="button_nav nav_right"></div>',
            prevText: '<div class="button_nav nav_left"></div>',
            minSlides: 1,
            maxSlides: 5,
            slideWidth: 180,
            slideMargin: 40,
            displaySlideQty: 1
        });
    });
</script>