<?php echo $header; ?>
<div class="container">
    <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a><i class="fa fa-long-arrow-right" aria-hidden="true"></i></li>
        <?php } ?>
    </ul>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
    <?php } ?>
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
    <?php } ?>
    <div class="row"><?php echo $column_left; ?>
        <?php if ($column_left && $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
        <?php $class = 'col-sm-9'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-12'; ?>
        <?php } ?>
        <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
            <div class="row">
                <div class="col-sm-6">
                    <div class="well">
                        <h2><?php echo $text_new_customer; ?></h2>
                        <p><strong><?php echo $text_register; ?></strong></p>
                        <p><?php echo $text_register_account; ?></p>
                        <a href="#" class="btn" data-toggle="modal" data-target="#modal_reg">Регистрация</a>
                        <a href="#" class="btn" data-toggle="modal" data-target="#modal_autorization">Войти</a>
                        <a href="<?php echo $register; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="well">
                        <h2><?php echo $text_returning_customer; ?></h2>
                        <p><strong><?php echo $text_i_am_returning_customer; ?></strong></p>
                        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <label class="control-label" for="input-email"><?php echo $entry_email; ?></label>
                                <input type="text" name="email" value="<?php echo $email; ?>" placeholder="<?php echo $entry_email; ?>" id="input-email" class="form-control" />
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="input-password"><?php echo $entry_password; ?></label>
                                <input type="password" name="password" value="<?php echo $password; ?>" placeholder="<?php echo $entry_password; ?>" id="input-password" class="form-control" />
                                <a href="<?php echo $forgotten; ?>"><?php echo $text_forgotten; ?></a>
                            </div>
                            <input type="submit" value="<?php echo $button_login; ?>" class="btn btn-primary" />
                            <?php if ($redirect) { ?>
                            <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
                            <?php } ?>
                        </form>
                    </div>
                </div>
            </div>
            <?php echo $content_bottom; ?>
        </div>
        <?php echo $column_right; ?>
    </div>
</div>

<!-- modal reg -->
<div class="modal fade" id="modal_reg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Регистрация</h4>
                <span class="modal_small_title">Уже зарегистрированы? <a href="#">Войдите</a></span>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <form id="form_registration">
                        <div id="message_reg"></div>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Имя" id="reg_name">
                            <div class="text-danger validate_modal">Введите имя</div>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Фамилия" id="reg_lastname">
                            <div class="text-danger validate_modal">Введите фамилию</div>
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control" placeholder="Email" id="reg_email">
                            <div class="text-danger validate_modal">Введите email</div>
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" placeholder="Пароль" id="reg_pass" >
                            <div class="text-danger validate_modal">Введите пароль</div>
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" placeholder="Подтвердите пароль" id="reg_pass2">
                            <div class="text-danger validate_modal">Введите пароль снова</div>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="+7 900 123 45-67" id="reg_tel">
                            <div class="text-danger validate_modal">Введите телефон</div>
                        </div>
                        <div class="checkboxFive">
                            <input type="checkbox" value="1" id="check_agreement" name="" />
                            <label for="check_agreement"></label>
                            <span>Принимаю условия <a href="/privacy" target="_blank">пользовательского соглашения</a></span>
                        </div>
                        <div class="modal_block_center text-center">
                            <input type="submit" id="submit_reg" class="btn_big_blue btn_blue_disabled" value="Зарегистрироваться">
                        </div>
                        </form>
                        <div class="block_social_signup">
                            <span>Войти с помощью социальных сетей</span>
                            <a href="#" class="btn_circle_border"><i class="fa fa-vk" aria-hidden="true"></i></a>
                            <a href="#" class="btn_circle_border"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                            <a href="#" class="btn_circle_border"><i class="fa fa-odnoklassniki" aria-hidden="true"></i></a>
                        </div>
                    </div>
                    <script>
                        $("#reg_tel").mask("+7 999 999-99-99");
                        function check_contact() {
                            if ( $("#check_agreement").is(":checked") ) {
                                $("#submit_reg").removeAttr("disabled");
                                $("#submit_reg").removeClass("btn_blue_disabled");
                            }     
                            else {
                                $("#submit_regsubmit_reg").attr("disabled", "disabled");
                                $("#submit_reg").addClass("btn_blue_disabled");
                            }
                        }
                        $("#check_agreement").click(function() {
                            check_contact();
                        });
                        $("#form_registration").submit(function() {
                            $("#form_registration input").each(function() {
                                if ($(this).val() == '') {
                                    $(this).siblings(".text-danger").show()
                                    $(this).addClass("no_valid");
                                } else {
                                    $(this).siblings(".text-danger").hide();
                                    $(this).removeClass("no_valid");
                                }
                            });
                            if ($("#reg_name").val() != '' && $("#reg_email").val() != '' && $("#reg_pass").val() != '' && $("#reg_pass2").val() != '' && $("#reg_tel").val() != '') {
                                $.ajax({
                                    type: "POST",
                                    url: "",
                                    data: $(this).serialize(),
                                    success: (function() {
                                        $("#form_registration").trigger("reset");
                                        $('#message_reg').append("<div class='alert alert-success'>Регистрация успешна.</div>");
                                    })
                                });
                            }
                            return false;
                        });
                    </script>
                </div>
            </div>
            <div class="modal-footer">
                <span class="small_text">Мы гарантирум то, что Ваши данные не будут использованы для рассылки нежелательной информации и ни при каких условиях не будут переданы третьим  лицам</span>
            </div>
        </div>
    </div>
</div>

<!-- modal autorization -->
<div class="modal fade" id="modal_autorization" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Личный кабинет</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <input type="text" name="email" value="<?php echo $email; ?>" placeholder="<?php echo $entry_email; ?>" id="input-email" class="form-control" />
                            </div>
                            <div class="form-group">
                                <input type="password" name="password" value="<?php echo $password; ?>" placeholder="<?php echo $entry_password; ?>" id="input-password" class="form-control" />
                            </div>
                            <div class="modal_autorization_block clearfix">
                                <div class="checkboxFive pull-left">
                                    <input type="checkbox" value="1" id="checkboxFiveInput" name="" />
                                    <label for="checkboxFiveInput"></label>
                                    <span>Запомнить меня</span>
                                </div>
                                <div class="pull-right">
                                    <a href="/forgot-password" class="recovery_pass">Забыли пароль?</a>
                                </div>    
                            </div>
                            <div class="modal_block_center text-center">
                                <input  type="submit" class="btn_big_blue" value="Войти">
                                <?php if ($redirect) { ?>
                                <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
                                <?php } ?>
                            </div>
                            <div class="block_social_signup">
                                <span>Войти с помощью социальных сетей</span>
                                <a href="#" class="btn_circle_border"><i class="fa fa-vk" aria-hidden="true"></i></a>
                                <a href="#" class="btn_circle_border"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                <a href="#" class="btn_circle_border"><i class="fa fa-odnoklassniki" aria-hidden="true"></i></a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <span class="modal_small_title">Еще не зарегистрированны? <a href="#" id="but_reg" data-toggle="modal" data-target="#modal_reg">Регистрация</a></span>
            </div>
        </div>
    </div>
</div>

<script>
    $("#but_reg").click(function() {
        $("#modal_autorization").modal("hide");
    })
</script>

<?php echo $footer; ?>