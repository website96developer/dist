<?php echo $header; ?>
<div class="container">
    <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a><i class="fa fa-long-arrow-right" aria-hidden="true"></i></li>
        <?php } ?>
    </ul>
    <div class="row"><?php echo $column_left; ?>
        <?php if ($column_left && $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
        <?php $class = 'col-sm-9'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-12'; ?>
        <?php } ?>
        <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
            <?php if ($orders) { ?>
            <div class="history_order_head">
                <div class="row">
                    <div class="col-md-8">
                        <span class="history_order_head">Всего заказов:</span>
                        <span><?php echo  count($orders); ?></span>
                    </div>
                    <div class="col-md-4 text-right history_order_restart">
                        <span onClick="location.reload();"><i class="fa fa-repeat" aria-hidden="true"></i>Обновить статус заказов</span>
                    </div>
                </div>
            </div>
            <div class="history_order_table">
                <div class="history_order_table_head">
                    <div class="row">
                        <div class="col-md-1">
                            Номер заказа
                        </div>
                        <div class="col-md-2">
                            Дата
                        </div>
                        <div class="col-md-2">
                            Тип получения
                        </div>
                        <div class="col-md-2">
                            Вид оплаты
                        </div>
                        <div class="col-md-1">
                            Сумма
                        </div>
                        <div class="col-md-2">
                            Статус заказа
                        </div>
                    </div>    
                </div>
                <?php foreach ($orders as $order) { ?>
                <div class="histoty_order_table_tr">
                    <div class="row">
                        <div class="col-md-1">
                            <a href="<?php echo $order['view']; ?>" data-toggle="tooltip" title="<?php echo $button_view; ?>" class="color_blue"><?php echo $order['order_id']; ?></a>
                        </div>
                        <div class="col-md-2">
                            <?php echo $order['date_added']; ?>
                        </div>
                        <div class="col-md-2">
                            Тип получения
                        </div>
                        <div class="col-md-2">
                            <?php echo $order['payment_method']; ?>
                        </div>
                        <div class="col-md-1">
                            <?php echo $order['total']; ?>
                        </div>
                        <div class="col-md-2">
                            <?php echo $order['status']; ?>
                        </div>                        
                    </div>
                </div>
                <?php } ?>
            </div>
            <div class="row">
                <div class="col-md-12 text-left"><?php echo $pagination; ?></div>
            </div>
            <?php } else { ?>
            <p><?php echo $text_empty; ?></p>
            <?php } ?>
            <?php echo $content_bottom; ?>
        </div>
        <?php echo $column_right; ?>
    </div>
</div>
<?php echo $footer; ?>
