<?php echo $header; ?>
<div class="container">
    <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a><i class="fa fa-long-arrow-right" aria-hidden="true"></i></li>
        <?php } ?>
    </ul>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
        <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="row"><?php echo $column_left; ?>
        <?php if ($column_left && $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
        <?php $class = 'col-sm-9'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-12'; ?>
        <?php } ?>
        <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
            <?php if ($products) { ?>
            <div class="favorite_header">
                <div class="row">
                    <div class="col-md-offset-4 col-md-8 text-right">
                        <div class="favorite_product_selected">
                            <span class="favorite_product">
                                Выбрано:
                                <span id="wishlist_count">0</span>
                            </span>
                            <div class="checkboxFive">
                                <input type="checkbox" value="1" name="" id="all_wishlist_product"/>
                                <label for="all_wishlist_product"></label>
                                <span>Выбрать все товары</span>
                            </div>
                            <a href="#" class="product_delete">Удалить</a>
                            <a href="#" class="btn">В корзину</a>    
                        </div>
                    </div>
                </div>    
            </div>
            <div class="row">
                <div class="col-md-12">
                    <ul class="product_carousel product_carouserl_favorite">
                        <?php foreach ($products as $product) { ?>
                            <li class="product-layout col-lg-4 col-md-4 col-sm-6 col-xs-12 list-unstyled">
                                <div class="product-thumb transition">
                                    <div class="checkboxFive">
                                        <input type="checkbox" class="wishlist_checbox" value="" id="favorite_tovar_<?php echo $product['product_id'] ?>" name="" />
                                        <label for="favorite_tovar_<?php echo $product['product_id'] ?>"></label>
                                    </div>
                                    <div class="image">
                                        <a href="<?php echo $product['href']; ?>">
                                            <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" />
                                        </a>
                                    </div>
                                    <div class="caption">
                                        <span class="product_title"><?php echo $product['name']; ?></span>
                                        <ul class="product_attributes_list list-unstyled">
                                        <?php foreach($product['attributes'] as $group_attr) {
                                            if($group_attr['attribute_group_id']==7) {
                                                foreach($group_attr['attribute'] as $attr) {
                                                    if($attr['attribute_id']==12||$attr['attribute_id']==17) {
                                                        ?>
                                                        <li class="product_attributes_item">
                                                            <span class="product_caption_title"><?php echo $attr['name']; ?></span>
                                                            <span><?php echo $attr['text']; ?></span>
                                                        </li>
                                                        <?
                                                    }
                                                }
                                            }
                                            ?>
                                        <?php } ?>
                                        </ul>
                                        <div class="product_caption_footer">
                                            <span class="product_count pull-left">
                                               <?php if ($product['price']) { ?>
                                                    <?php if (!$product['special']) { ?>
                                                    <?php echo $product['price']; ?>
                                                    <i class="fa fa-rub" aria-hidden="true"></i>
                                                    <?php } else { ?>
                                                    <span class="price-new"><?php echo $product['special']; ?><i class="fa fa-rub" aria-hidden="true"></i></span><span class="price-old"><?php echo $product['price']; ?><i class="fa fa-rub" aria-hidden="true"></i></span>
                                                    <?php } ?>
                                                <?php } ?>
                                            </span>
                                            <div class="product_caption_buy pull-rigth">
                                                <button onclick="cart.add('<?php echo $product['product_id']; ?>');" class="btn">В корзину</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        <?php } ?>    
                    </ul>
                </div>
            </div>
          <?php } else { ?>
          <p><?php echo $text_empty; ?></p>
          <?php } ?>
            <?php echo $content_bottom; ?>
        </div>
    <?php echo $column_right; ?>
    </div>
</div>
<?php echo $footer; ?>