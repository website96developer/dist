<style media='print' type='text/css'>
    header, .header_big, #top, .header_small, footer, .breadcrumb { display: none; opacity: 0; height: 0; }
</style>
<?php echo $header; ?>
<div class="container">
    <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a><i class="fa fa-long-arrow-right" aria-hidden="true"></i></li>
        <?php } ?>
    </ul>
    <?php if ($success) { ?>
        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
    <?php } ?>
    <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
    <?php } ?>
    <div class="row"><?php echo $column_left; ?>
        <?php if ($column_left && $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
        <?php $class = 'col-sm-9'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-12'; ?>
        <?php } ?>
        <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
<!--
            <ul class="nav nav-tabs">
                <li><a href="#profile" data-toggle="tab">Персональные данные</a></li>
                <li><a href="#product" data-toggle="tab">Отложенные товары</a></li>
                <li class="active"><a href="#history" data-toggle="tab">История заказов</a></li>
            </ul>
-->
            <a href="#" onClick="history.back()" class="history_back"><i class="fa fa-long-arrow-left" aria-hidden="true"></i>Вернуться к списку заказов</a>
            <h2>Заказ №<?php echo $order_id; ?>:<span class="status_order"><?php echo $status; ?></span></h2>
            <span class="order_info_title">Информация о заказе</span>
            <div class="row">
               <div class="col-md-8">
                    <ul class="order_info_list list-unstyled">
                        <li class="order_info_item">
                            <span class="text-left txt"><span>Получатель</span></span>
                            <span><?php echo $shipping_address['firstname']," ", $shipping_address['lastname']; ?></span>
                        </li>
                        <li class="order_info_item">
                            <span class="text-left txt"><span>Телефон</span></span>
                            <span><?php echo $telephone; ?></span>
                        </li>
                        <li class="order_info_item">
                            <span class="text-left txt"><span>Электронная почта</span></span>
                            <span><?php echo $email; ?></span>
                        </li>
                        <li class="order_info_item">
                            <span class="text-left txt"><span>Город</span></span>
                            <span><?php echo $shipping_address['city']; ?></span>
                        </li>
                        <li class="order_info_item">
                            <span class="text-left txt"><span>Тип получения</span></span>
                            <span><?php echo $shipping_method; ?></span>
                        </li>
                        <li class="order_info_item">
                            <span class="text-left txt"><span>Вид оплаты</span></span>
                            <span><?php echo $payment_method; ?></span>
                        </li>
                        <li class="order_info_item">
                            <span class="text-left txt"><span>Адрес доставки</span></span>
                            <span><?php echo $shipping_address['address_1']; ?></span>
                        </li>
                    </ul>
                </div>
            </div>
            <span class="order_info_title">Состав заказа</span>
            <div class="row">
                <div class="col-md-8">
                    <div class="table_cart_head">
                        <div class="row">
                            <div class="text-left col-md-7">
                                <div class="table_cart_title">
                                    Наименование
                                </div>
                            </div>
                            <div class="text-center col-md-2">
                                <div class="table_cart_title">
                                    Кол-во
                                </div>
                            </div>
                            <div class="text-center col-md-3">
                                <div class="table_cart_title">
                                    Стоимость
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php foreach ($products as $product) { ?>
                    <div class="table_cart_tr">
                        <div class="row">
                            <div class="text-center col-md-2">
                                <a href="<?php echo $product['href']; ?>" class="cart_img_link">
                                    <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-thumbnail" />
                                </a>
                            </div>
                            <div class="text-left col-md-5">
                                <a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                                    <?php foreach($product['attributes'] as $group_attr) {
									    if($group_attr['attribute_group_id']==7) {
										    foreach($group_attr['attribute'] as $attr) {
											    if($attr['attribute_id']==12) {
												    ?>
												<span class="table_cart_tr_title"><?php echo $attr['name']; ?></span>
												<span class="table_cart_tr_text"><?php echo $attr['text']; ?></span>
												<?
											    }
										    }
									    }
								    ?>
								<?php } ?>
                            </div>
                            <div class="text-left col-md-2 text-center table_cart_count">
                                <input id="count_product" class="form-control" type="number" value="<?php echo $product['quantity']; ?>">
                            </div>
                            <div class="text-center col-md-3 table_cart_price">
                                <?php echo $product['price']; ?><i class="fa fa-rub" aria-hidden="true"></i>
                            </div>
                       </div>
                    </div>
                    <div class="order_info_footer">
                        <div class="row">
                            <div class="col-md-6">
                                <span onclick="window.print();"><i class="fa fa-print" aria-hidden="true"></i>Распечатать</span>
                                <a href="#"><i class="fa fa-paper-plane-o" aria-hidden="true"></i>Отправить заказ на почту</a>
                            </div>
                            <div class="col-md-6">
                                <ul class="order_footer_price_list list-unstyled">
                                    <?php foreach ($totals as $total) { ?>
                                    <li class="order_footer_price_item">
                                        <span class="price_item_title"><?php echo $total['title']; ?>:</span>
                                        <span class="price_item_value"><?php echo $total['text']; ?><i class="fa fa-rub" aria-hidden="true"></i></span>
                                    </li>
                                    <?php } ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
            <?php echo $content_bottom; ?>
        </div>
        <?php echo $column_right; ?>
    </div>
</div>
<?php echo $footer; ?>