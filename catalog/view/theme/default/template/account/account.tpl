<?php echo $header; ?>
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a><i class="fa fa-long-arrow-right" aria-hidden="true"></i></li>
    <?php } ?>
  </ul>
  <?php if ($success) { ?>
  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
  <?php } ?>
  <div class="row"><?php echo $column_left; ?>
        <?php if ($column_left && $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
        <?php $class = 'col-sm-9'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-12'; ?>
        <?php } ?>
        <div class="col-md-12">
            <span class="category_title">Личный кабинет</span>
            <div class="user_hello">
                <div class="user_hello_left">
                    <span class="btn_circle_background_small icon_user"></span>
                    <span class="user_hello_text">Здравствуйте, <span><?php echo $firstname; ?></span>, добро пожаловать в личный кабинет</span>
                </div>
                <div class="user_hello_right_column pull-right">
                    <a href="/logout" class="user_hello_exit">
                        <span class="table_cart_tr_delete"><i class="fa fa-times" aria-hidden="true"></i></span>
                        <span>Выйти</span>
                    </a>
                </div>  
            </div> 
        </div>
    </div>
    <div class="row">
        <div id="content" class="col-md-12"><?php echo $content_top; ?>
            <ul class="nav nav-tabs">
                <li class="active"><a href="#profile" data-toggle="tab">Персональные данные</a></li>
                <li><a href="#product" data-toggle="tab">Отложенные товары</a></li>
                <li><a href="#history" data-toggle="tab">История заказов</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="profile">
                    <div class="row">
                        <div class="col-md-8">
                            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="account_edit">
                                <span class="title_cart">Личная информация</span>
                                <div class="clearfix">
                                    <div class="profile_avatar pull-left">
                                        <img src="catalog/view/theme/default/image/user.png" alt="" class="src">
                                    </div>
                                    <span class="profile_avatar_text">
                                        <a href="#">Добавить аватар</a>
                                        <span>jpeg, jpeg, png до 900 Кб.</span>
                                    </span>
                                </div>
                                <div class="cart_block">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="name"><?php echo $entry_firstname; ?></label>
                                                <input type="text" name="firstname" value="<?php echo $firstname; ?>" placeholder="<?php echo $entry_firstname; ?>" id="input-firstname" class="form-control">
                                                <?php if ($error_firstname) { ?>
                                                <div class="text-danger"><?php echo $error_firstname; ?></div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="name"><?php echo $entry_lastname; ?></label>
                                                <input type="text" name="lastname" value="<?php echo $lastname; ?>" placeholder="<?php echo $entry_lastname; ?>" id="input-lastname" class="form-control">
                                                <?php if ($error_lastname) { ?>
                                                <div class="text-danger"><?php echo $error_lastname; ?></div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="name"><?php echo $entry_telephone; ?></label>
                                                <input type="text" name="telephone" value="<?php echo $telephone; ?>" placeholder="<?php echo $entry_telephone; ?>" id="input-telephone" class="form-control">
                                                <?php if ($error_telephone) { ?>
                                                <div class="text-danger"><?php echo $error_telephone; ?></div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="name"><?php echo $entry_email; ?></label>
                                                <input type="email" name="email" value="<?php echo $email; ?>" placeholder="<?php echo $entry_email; ?>" id="input-email" class="form-control">
                                                <?php if ($error_email) { ?>
                                                <div class="text-danger"><?php echo $error_email; ?></div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="cart_block">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <span class="title_cart">Адрес доставки</span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="name">Улица<span class="form_group_star">*</span></label>
                                                <input type="text" class="form-control" placeholder="" id="street">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="name">Дом<span class="form_group_star">*</span></label>
                                                <input type="text" class="form-control" placeholder="" id="house">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="name">Корпус</label>
                                                <input type="text" class="form-control" placeholder="" id="housing">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="name">Квартира<span class="form_group_star">*</span></label>
                                                <input type="number" class="form-control" placeholder="" id="apartment">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="cart_block">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <span class="title_cart">Смена пароля</span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="old_pass">Старый пароль</label>
                                                <input type="password" class="form-control" placeholder="" id="old_pass">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="new_pass">Новый пароль</label>
                                                <input type="password" class="form-control" placeholder="" id="new_pass" name="password" value="<?php echo $password; ?>">
                                                <?php if ($error_password) { ?>
                                                <div class="text-danger"><?php echo $error_password; ?></div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="new_pass2">Подтвердите пароль</label>
                                                <input type="password" class="form-control" placeholder="" id="new_pass2" name="confirm" value="<?php echo $confirm; ?>">
                                                <?php if ($error_confirm) { ?>
                                                <div class="text-danger"><?php echo $error_confirm; ?></div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="profile_save">
                                    <button type="submit" class="btn_big_blue">Сохранить изменения</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="product">
                    <div class="row">
                        <div class="col-md-6">
                             <span class="favorite_product">
                                Выбрано:
                                <span id="wishlist_count">0</span>
                            </span>
                            <div class="checkboxFive">
                                <input type="checkbox" value="1" name="" id="all_wishlist_product"/>
                                <label for="all_wishlist_product"></label>
                                <span>Выбрать все товары</span>
                            </div>
                        </div>
                        <div class="col-md-6 text-right">
                            <a href="#" class="product_delete">Удалить</a>
                            <a href="#" class="btn">В корзину</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <ul class="product_carousel product_carouserl_favorite">
                                <?php foreach ($products as $product) { ?>
                                    <li class="product-layout col-lg-4 col-md-4 col-sm-6 col-xs-12 list-unstyled">
                                        <div class="product-thumb transition">
                                            <div class="checkboxFive">
                                                <input type="checkbox" class="wishlist_checbox" value="" id="favorite_tovar_<?php echo $product['product_id'] ?>" name="" />
                                                <label for="favorite_tovar_<?php echo $product['product_id'] ?>"></label>
                                            </div>
                                            <div class="image">
                                                <a href="<?php echo $product['href']; ?>">
                                                    <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" />
                                                </a>
                                            </div>
                                            <div class="caption">
                                                <span class="product_title"><?php echo $product['name']; ?></span>
                                                <ul class="product_attributes_list list-unstyled">
                                                <?php foreach($product['attributes'] as $group_attr) {
                                                    if($group_attr['attribute_group_id']==7) {
                                                        foreach($group_attr['attribute'] as $attr) {
                                                            if($attr['attribute_id']==12||$attr['attribute_id']==17) {
                                                                ?>
                                                                <li class="product_attributes_item">
                                                                    <span class="product_caption_title"><?php echo $attr['name']; ?></span>
                                                                    <span><?php echo $attr['text']; ?></span>
                                                                </li>
                                                                <?php
                                                            }
                                                        }
                                                    }
                                                    ?>
                                                <?php } ?>
                                                </ul>
                                                <div class="product_caption_footer">
                                                    <span class="product_count pull-left">
                                                       <?php if ($product['price']) { ?>
                                                            <?php if (!$product['special']) { ?>
                                                            <?php echo $product['price']; ?>
                                                            <i class="fa fa-rub" aria-hidden="true"></i>
                                                            <?php } else { ?>
                                                            <span class="price-new"><?php echo $product['special']; ?><i class="fa fa-rub" aria-hidden="true"></i></span><span class="price-old"><?php echo $product['price']; ?><i class="fa fa-rub" aria-hidden="true"></i></span>
                                                            <?php } ?>
                                                        <?php } ?>
                                                    </span>
                                                    <div class="product_caption_buy pull-rigth">
                                                        <button onclick="cart.add('<?php echo $product['product_id']; ?>');" class="btn">В корзину</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                <?php } ?>    
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="history">
                    <?php if ($orders) { ?>
                    <div class="history_order_head">
                        <div class="row">
                            <div class="col-md-8">
                                <span class="history_order_head">Всего заказов:</span>
                                <span><?php echo  count($orders); ?></span>
                            </div>
                            <div class="col-md-4 text-right history_order_restart">
                                <span onClick="location.reload();"><i class="fa fa-repeat" aria-hidden="true"></i>Обновить статус заказов</span>
                            </div>
                        </div>
                    </div>
                    <div class="history_order_table">
                        <div class="history_order_table_head">
                            <div class="row">
                                <div class="col-md-1">
                                    Номер заказа
                                </div>
                                <div class="col-md-2">
                                    Дата
                                </div>
                                <div class="col-md-2">
                                    Тип получения
                                </div>
                                <div class="col-md-2">
                                    Вид оплаты
                                </div>
                                <div class="col-md-1">
                                    Сумма
                                </div>
                                <div class="col-md-3">
                                    Статус заказа
                                </div>
                            </div>    
                        </div>
                        <?php foreach ($orders as $order) { ?>
                        <div class="histoty_order_table_tr">
                            <div class="row">
                                <div class="col-md-1">
                                    <a href="<?php echo $order['view']; ?>" data-toggle="tooltip" title="<?php echo $button_view; ?>" class="color_blue"><?php echo $order['order_id']; ?></a>
                                </div>
                                <div class="col-md-2">
                                    <?php echo $order['date_added']; ?>
                                </div>
                                <div class="col-md-2">
                                    <?php echo $order['shipping_method']; ?>
                                </div>
                                <div class="col-md-2">
                                    <?php echo $order['payment_method']; ?>
                                </div>
                                <div class="col-md-1">
                                    <?php echo $order['total']; ?>
                                </div>
                                <div class="col-md-3">
                                    <?php
                                        switch ($order['status']) {
                                            case "Отменен":
                                                ?><span style='color: red;'><?php echo $order['status'];?></span><?php
                                                break;
                                            case "Ожидается подтверждение":
                                                ?><span style='color: #a6adc3;'><?php echo $order['status'];?></span><?php
                                                break;
                                            case "Доставлен":
                                                ?><span style='color: #00c665;'><?php echo $order['status'];?></span><?php
                                                break;
                                            default:
                                                ?><span style='color: #333;'><?php echo $order['status'];?></span><?php
                                                break;
                                            
                                        }
                                        ?>
                                </div>                        
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-left"><?php echo $pagination; ?></div>
                    </div>
                    <?php } else { ?>
                    <p><?php echo $text_empty; ?></p>
                    <?php } ?>
                </div>
            </div>
        <?php echo $content_bottom; ?>
        </div>
        <?php echo $column_right; ?>
    </div>
</div>
<script>
    $("#input-telephone").mask("+7 999 999-99-99");
    $("#account_edit .form-group").each(function() {
        if ($(this).find(".text-danger").is(":visible")) {
            $(this).find("input, textarea").addClass("no_valid");
        } else {
            $(this).find("input, textarea").removeClass("no_valid");
        }
    });
</script>
<?php echo $footer; ?> 