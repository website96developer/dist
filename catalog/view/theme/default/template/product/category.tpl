<?php echo $header; ?>
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a><i class="fa fa-long-arrow-right" aria-hidden="true"></i></li>
    <?php } ?>
  </ul>
  <div class="row">
      <div class="col-md-12">
          <h2 class="category_title"><?php echo $heading_title; ?></h2>
      </div>
  </div>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <div class="row content_top">
          <div class="col-md-10">
              <span>Сортировать:</span>
              <ul class="content_sort list-unstyled list-inline">
                <?php foreach ($sorts as $sorts) { ?>
                    <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
                        <li class="content_sort_item">
                            <a href="<?php echo $sorts['href']; ?>" id="sort_active"><?php echo $sorts['text']; ?>
                            <?php if (isset($_GET['sort']) && $_GET['order'] == "DESC") { ?>
                                <i class="fa fa-sort-amount-asc" aria-hidden="true"></i>
                            <?php } else { ?>
                                <i class="fa fa-sort-amount-desc" aria-hidden="true"></i>
                            <?php } ?>
                            </a>
                        </li>
                    <?php } else { ?>
                        <li class="content_sort_item"><a href="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></a></li>
                    <?php } ?>
                <?php } ?>
              </ul>
          </div>
          <div class="col-md-2 text-right">
              <span class="count_search">Найдено <?php echo $results; ?></span>
          </div>
      </div>
      <?php if ($products) { ?>
        <div class="row">
            <div class="col-md-12">
                <ul class="product_carousel product_carousel_3 clearfix">
                <?php foreach ($products as $product) { ?>
                    <li class="product-layout list-unstyled list-inline">
                        <div class="product-thumb transition">
                            <div class="image">
                                <a href="<?php echo $product['href']; ?>">
                                    <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" />
                                </a>
                            </div>
                            <div class="caption">
                                <span class="product_title"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></span>
                                <ul class="product_attributes_list list-unstyled">
                                    <li class="product_attributes_item">
                                        <span class="product_caption_title">Автор</span>
                                        <span>Человек</span>
                                    </li>
                                    <li class="product_attributes_item">
                                        <span class="product_caption_title">Жанр</span>
                                        <span>Современная проза</span>
                                    </li>
                                </ul>
                                <div class="product_caption_footer">
                                    <span class="product_count pull-left">
                                    <?php if ($product['price']) { ?>
                                    <?php if (!$product['special']) { ?>
                                    <?php echo $product['price']; ?>
                                    <i class="fa fa-rub" aria-hidden="true"></i>
                                    <?php } else { ?>
                                    <span class="price-new"><?php echo $product['special']; ?><i class="fa fa-rub" aria-hidden="true"></i></span><span class="price-old"><?php echo $product['price']; ?><i class="fa fa-rub" aria-hidden="true"></i></span>
                                    <?php } ?>
                                    <?php if ($product['tax']) { ?>
                                    <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?><i class="fa fa-rub" aria-hidden="true"></i></span>
                                    <?php } ?>
                                    <?php } ?>
                                    </span>
                                    <div class="product_caption_buy pull-rigth">
                                    <button onclick="cart.add('<?php echo $product['product_id']; ?>');" class="btn">В корзину</button>
                                    <button onclick="wishlist.add('<?php echo $product['product_id']; ?>');" class="btn_circle_background_small btn_wishlist"><i class="fa fa-star-o" aria-hidden="true"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                <?php } ?>
                </ul>
            </div>
        </div>
        <?php if (!empty($pagination)) { ?>
        <div class="row">
            <div class="col-md-12 text-center pagination_wrapper">
                <?php echo $pagination; ?>
            </div>
        </div>
        <?php } ?>
      <?php } ?>
      <?php if (!$categories && !$products) { ?>
      <p><?php echo $text_empty; ?></p>
      <div class="buttons">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
      </div>
      <?php } ?>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>
