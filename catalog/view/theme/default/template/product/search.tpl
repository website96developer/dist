<?php echo $header; ?>
<div class="container">
<ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a><i class="fa fa-long-arrow-right" aria-hidden="true"></i></li>
    <?php } ?>
</ul>
    <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
        <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
        <h1 class="title_result_search"><?php echo $heading_title; ?></h1>
        <div class="row">
            <div class="col-md-12">
                <div class="search_main">
                    <input type="text" name="search" value="<?php echo $search; ?>" placeholder="<?php echo $text_search; ?>" class="form-control input-lg" />
                    <span class="search_select"><span id="search_select_text">Искать везде</span>
                        <div class="search_select_box">
                            <ul class="search_select_list list-unstyled">
                                <li class="search_select_item">
                                    <span>Искать везде</span>
                                </li>
                                <li class="search_select_item">
                                    <span>Искать по книгам</span>
                                </li>
                                <li class="search_select_item">
                                    <span>Искать по товарам</span>
                                </li>
                            </ul>
                        </div>
                    </span>
                    <button type="button" class="btn btn-default btn-lg"></button>
                </div>
                <?php if ($products) { ?>
                <div class="search_item_count">
                    <span>По вашему запросу найдено</span>
                    <span><span id="search_result_count"><?php echo $results; ?></span>результатов</span>  
                </div>
                <?php } ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <?php if ($products) { ?>
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#profile" data-toggle="tab">Книги</a></li>
                    <li><a href="#product" data-toggle="tab">Другие товары</a></li>
                    <li><a href="#history" data-toggle="tab">Страницы</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="profile">
                        <div class="row content_top">
                            <div class="col-md-8">
                                <span>Сортировать:</span>
                                <ul class="content_sort list-unstyled list-inline">
                                    <?php foreach ($sorts as $sorts) { ?>
                                        <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
                                            <li class="content_sort_item">
                                                <a href="<?php echo $sorts['href']; ?>" id="sort_active"><?php echo $sorts['text']; ?>
                                                <?php if (isset($_GET['sort']) && $_GET['order'] == "DESC") { ?>
                                                    <i class="fa fa-sort-amount-asc" aria-hidden="true"></i>
                                                <?php } else { ?>
                                                    <i class="fa fa-sort-amount-desc" aria-hidden="true"></i>
                                                <?php } ?>
                                                </a>
                                            </li>
                                        <?php } else { ?>
                                            <li class="content_sort_item"><a href="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></a></li>
                                        <?php } ?>
                                    <?php } ?>
                                </ul>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <ul class="product_carousel product_carousel_4 clearfix">
                                <?php foreach ($products as $product) { ?>
                                    <li class="product-layout list-unstyled list-inline">
                                        <div class="product-thumb transition">
                                            <div class="image">
                                                <a href="<?php echo $product['href']; ?>">
                                                    <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" />
                                                </a>
                                            </div>
                                            <div class="caption">
                                                <span class="product_title"><?php echo $product['name']; ?></span>
                                                <ul class="product_attributes_list list-unstyled">
                                                    <li class="product_attributes_item">
                                                        <span class="product_caption_title">Автор</span>
                                                        <span>Человек</span>
                                                    </li>
                                                    <li class="product_attributes_item">
                                                        <span class="product_caption_title">Жанр</span>
                                                        <span>Современная проза</span>
                                                    </li>
                                                </ul>
                                                <div class="product_caption_footer">
                                                    <span class="product_count pull-left">
                                                    <?php if ($product['price']) { ?>
                                                    <?php if (!$product['special']) { ?>
                                                    <?php echo $product['price']; ?>
                                                    <i class="fa fa-rub" aria-hidden="true"></i>
                                                    <?php } else { ?>
                                                    <span class="price-new"><?php echo $product['special']; ?><i class="fa fa-rub" aria-hidden="true"></i></span><span class="price-old"><?php echo $product['price']; ?><i class="fa fa-rub" aria-hidden="true"></i></span>
                                                    <?php } ?>
                                                    <?php if ($product['tax']) { ?>
                                                    <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?><i class="fa fa-rub" aria-hidden="true"></i></span>
                                                    <?php } ?>
                                                    <?php } ?>
                                                    </span>
                                                    <div class="product_caption_buy pull-rigth">
                                                    <a href="#" onclick="cart.add('<?php echo $product['product_id']; ?>');" class="btn">В корзину</a>
                                                    <a href="#" onclick="wishlist.add('<?php echo $product['product_id']; ?>');" class="btn_circle_background_small"><i class="fa fa-star-o" aria-hidden="true"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                <?php } ?>
                                </ul>
                            </div> 
                        </div>
                    </div>
                    <div class="tab-pane" id="product">
                        <div class="row content_top">
                            <div class="col-md-8">
                                <span>Сортировать:</span>
                                <ul class="content_sort list-unstyled list-inline">
                                    <li class="content_sort_item">по статусу</li>
                                    <li class="content_sort_item">по новизне</li>
                                    <li class="content_sort_item">по стоимости</li>
                                </ul>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <ul class="product_carousel product_carousel_4 clearfix">
                                <?php foreach ($products as $product) { ?>
                                    <li class="product-layout list-unstyled list-inline">
                                        <div class="product-thumb transition">
                                            <div class="image">
                                                <a href="<?php echo $product['href']; ?>">
                                                    <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" />
                                                </a>
                                            </div>
                                            <div class="caption">
                                                <span class="product_title"><?php echo $product['name']; ?></span>
                                                <ul class="product_attributes_list list-unstyled">
                                                    <li class="product_attributes_item">
                                                        <span class="product_caption_title">Автор</span>
                                                        <span>Человек</span>
                                                    </li>
                                                    <li class="product_attributes_item">
                                                        <span class="product_caption_title">Жанр</span>
                                                        <span>Современная проза</span>
                                                    </li>
                                                </ul>
                                                <div class="product_caption_footer">
                                                    <span class="product_count pull-left">
                                                    <?php if ($product['price']) { ?>
                                                    <?php if (!$product['special']) { ?>
                                                    <?php echo $product['price']; ?>
                                                    <i class="fa fa-rub" aria-hidden="true"></i>
                                                    <?php } else { ?>
                                                    <span class="price-new"><?php echo $product['special']; ?><i class="fa fa-rub" aria-hidden="true"></i></span><span class="price-old"><?php echo $product['price']; ?><i class="fa fa-rub" aria-hidden="true"></i></span>
                                                    <?php } ?>
                                                    <?php if ($product['tax']) { ?>
                                                    <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?><i class="fa fa-rub" aria-hidden="true"></i></span>
                                                    <?php } ?>
                                                    <?php } ?>
                                                    </span>
                                                    <div class="product_caption_buy pull-rigth">
                                                    <a href="#" onclick="cart.add('<?php echo $product['product_id']; ?>');" class="btn">В корзину</a>
                                                    <a href="#" onclick="wishlist.add('<?php echo $product['product_id']; ?>');" class="btn_circle_background_small"><i class="fa fa-star-o" aria-hidden="true"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                <?php } ?>
                                </ul> 
                            </div>    
                        </div>
                    </div>
                    <div class="tab-pane" id="history">

                    </div>
                </div>
                <?php } else { ?>
                    <p><?php echo $text_empty; ?></p>
                <?php } ?>
            </div>
        </div>
        <?php if (!empty($pagination)) { ?>
        <div class="row">
            <div class="col-md-12 text-center pagination_wrapper">
                <?php echo $pagination; ?>
            </div>
        </div>
        <?php } ?>
        </div>
    <?php echo $column_right; ?>
    </div>
</div>
<script type="text/javascript"><!--
$('#button-search').bind('click', function() {
	url = 'index.php?route=product/search';

	var search = $('#content input[name=\'search\']').prop('value');

	if (search) {
		url += '&search=' + encodeURIComponent(search);
	}

	var category_id = $('#content select[name=\'category_id\']').prop('value');

	if (category_id > 0) {
		url += '&category_id=' + encodeURIComponent(category_id);
	}

	var sub_category = $('#content input[name=\'sub_category\']:checked').prop('value');

	if (sub_category) {
		url += '&sub_category=true';
	}

	var filter_description = $('#content input[name=\'description\']:checked').prop('value');

	if (filter_description) {
		url += '&description=true';
	}

	location = url;
});

$('#content input[name=\'search\']').bind('keydown', function(e) {
	if (e.keyCode == 13) {
		$('#button-search').trigger('click');
	}
});

$('select[name=\'category_id\']').on('change', function() {
	if (this.value == '0') {
		$('input[name=\'sub_category\']').prop('disabled', true);
	} else {
		$('input[name=\'sub_category\']').prop('disabled', false);
	}
});

$('select[name=\'category_id\']').trigger('change');
--></script>
<?php echo $footer; ?>