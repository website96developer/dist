<?php if ($reviews) { ?>
<?php foreach ($reviews as $review) { ?>
<li class="feedback_item">
    <div class="clearfix">
        <div class="feedback_user">
            <span class="feedback_user_avatar pull-left">
                <img src="catalog/view/theme/default/image/user.png" alt="">
            </span>
            <span class="feedback_user_desc">
                <span><?php echo $review['author']; ?></span>
                <span><i class="fa fa-clock-o" aria-hidden="true"></i><?php echo $review['date_added']; ?></span>
            </span>
        </div>
        <div class="feedback_stars pull-right">
            <div class="star_product">
                <?php for ($i = 1; $i <= 5; $i++) { ?>
                <?php if ($review['rating'] < $i) { ?>
                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                <?php } else { ?>
                <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i></span>
                <?php } ?>
                <?php } ?>
            </div>
            <span>Оценка:</span>
            <span><?php echo $review['rating']; ?></span>
        </div>
    </div>
    <div class="feedback_text">
        <?php echo $review['text']; ?>
    </div>
</li>
<?php } ?>
<div class="text-right">
    <?php echo $pagination; ?>
</div>
<?php } else { ?>
<p><?php echo $text_no_reviews; ?></p>
<?php } ?>

<script>
    $(".feedback_text").each(function() {
        if ($(this).text().length > 400) {
            console.log("ff");
            $(this).addClass("feedback_text_wrap");
        }
        else {
            $(this).removeClass("feedback_text_wrap");
        }    
    });
    $().ready(function() {
     $('.feedback_text').truncate( {
      length: 400,
      minTrail: 10,
      moreText: 'Читать полностью',
      lessText: 'Скрыть',
      ellipsisText: ""
     });
    });
</script>