<?php echo $header; ?>
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a><i class="fa fa-long-arrow-right" aria-hidden="true"></i></li>
    <?php } ?>
  </ul>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <div class="row">
        <?php if ($column_left || $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-8'; ?>
        <?php } ?>
        <div class="col-md-9">
            <h1><?php echo $heading_title; ?></h1>
            <div class="product_header">
                <span class="product_header_title_id">Код товара</span>
                <span class="product_header_id"><?php echo $text_model; ?> <?php echo $model; ?></span>
                <i class="fa fa-check-circle product_header_have" aria-hidden="true"></i>
                <span class="product_header_have"><?php echo $stock; ?></span>
                <div class="star_product">
                    <?php for ($i = 1; $i <= 5; $i++) { ?>
                    <?php if ($rating < $i) { ?>
                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
                    <?php } else { ?>
                    <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                    <?php } ?>
                    <?php } ?>
                </div>
                <span><?php echo $reviews; ?></span>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-9">
           <div class="product_content">
                <div class="row" id="product">
                    <div class="col-md-4">
                        <?php if ($thumb || $images) { ?>
                        <ul class="thumbnails">
                            <?php if ($thumb) { ?>
                            <li>
                                <a class="thumbnail" href="<?php echo $popup; ?>" title="<?php echo $heading_title; ?>">
                                    <img src="<?php echo $thumb; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" />
                                </a>
                            </li>
                            <?php } ?>
                            <?php if ($images) { ?>
                            <?php foreach ($images as $image) { ?>
                            <li class="image-additional">
                                <a class="thumbnail" href="<?php echo $image['popup']; ?>" title="<?php echo $heading_title; ?>">
                                <img src="<?php echo $image['thumb']; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" />
                                </a>
                            </li>
                            <?php } ?>
                            <?php } ?>
                        </ul>
                        <?php } ?>
                    </div>
                    <div class="col-md-8">
                        <ul class="product_atributes_list list-unstyled">
                            <?php foreach($attribute_groups as $attr) {
								foreach($attr['attribute'] as $a) { 
									if (!strripos($a['text'],'|'))
									{ ?>
										<li class="product_atributes_item">
										    <span class="text-left txt">
                                                <span><?php echo $a['name'];?></span>
                                            </span>
										    <span><?php echo $a['text'];?></span>
								        </li>
								    <?php } ?> 
								<?php } ?> 
							<?php } ?> 
                        </ul>
                        <ul class="product_atributes_list_product_hidden list-unstyled">
                            <li class="product_atributes_item">
                                <span>
                                    <?php echo $description; ?>
                                </span>
                            </li>
                        </ul>
                        <span class="all_atributes">
                            Показать описание<i class="fa fa-angle-down" aria-hidden="true"></i>
                        </span>
                        <div class="full_product">
                            <div class="pull-left full_product_price">
                                <?php if ($price) { ?>
                                <?php if (!$special) { ?>
                                <span class="product_new_price"><?php echo $price; ?><i class="fa fa-rub"></i></span>
                                <?php } else { ?>
                                <span class="product_new_price"><?php echo $special; ?><i class="fa fa-rub"></i></span>
                                <span class="product_odl_price"><?php echo $price; ?><i class="fa fa-rub"></i></span>
                                <?php } ?>  
                                <?php } ?>  
                            </div>
                            <span class="pull-right">
                                <input type="text" name="quantity" value="<?php echo $minimum; ?>" size="2" id="input-quantity" class="form-control hidden" />
                                <input type="hidden" name="product_id" value="<?php echo $product_id; ?>" />
                                <button class="btn_big_blue" type="submit" id="button-cart" onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');" data-loading-text="<?php echo $text_loading; ?>">Добавить в корзину</button>
                                <button class="btn_circle_background_big" onclick="wishlist.add('<?php echo $product_id; ?>');"><span class="btn_pic btn_pic_star"></span></button>
                                <button id="share_show" class="btn_circle_background_big">
                                    <span class="btn_pic_share"></span>
                                    <div class="share_block">
                                        <ul class="list-unstyled share_list">
                                            <li class="share_item">
                                                <a target="_blank" href="http://vk.com/share.php?url=<?php echo $hrefs; ?>&title=<?php echo $heading_title; ?>" class="share_link share_vk">Вконтакте</a>
                                            </li>
                                            <li class="share_item">
                                                <a target="_blank" href="http://www.facebook.com/sharer.php?u=<?php echo $hrefs; ?>" class="share_link share_fb">Facebook</a>
                                            </li>
                                            <li class="share_item">
                                                <a target="_blank" href="http://www.ok.ru/dk?st.cmd=addShare&st.s=1&st._surl=<?php echo $hrefs; ?>" class="share_link share_odn">Одноклассники</a>
                                            </li>
                                        </ul>
                                    </div>
                                </button>
                            </span>
                        </div>
                    </div>
            </div>
        </div>
        <div class="feedback_header">
            <div class="row">
                <div class="col-md-12">
                    <span class="title_feedback">Отзывы о книге</span>
                    <span class="count_feedback"><?php echo $tab_review; ?></span>
                    <div class="pull-right">
                        <ul class="content_sort list-unstyled list-inline">
                            <li class="content_sort_item">по дате</li>
                            <li class="content_sort_item">по количеству звезд</li>
                            <li class="content_sort_item">по оценке отзыва</li>
                        </ul>
                    </div>
                </div>
            </div>
            <?php if (!($review_guest)) { ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="feedback_authorization">
                        <i class="fa fa-commenting-o" aria-hidden="true"></i>
                        <a href="/login">Авторизоваться и написать отзыв</a>
                    </div>
                </div>
            </div>
            <?php } else { ?>
            <form class="form-horizontal" id="form-review">
                <div class="form-group required hidden">
                    <div class="col-sm-12">
                    <label class="control-label" for="input-name"><?php echo $entry_name; ?></label>
                    <input type="text" name="name" value="<?php echo $customer_name; ?>" id="input-name" class="form-control" />
                    </div>
                </div>
                <div class="form-group required">
                    <div class="col-sm-12">
                        <div class="wrapper_textarea"><textarea name="text" rows="5" id="input-review" class="form-control" placeholder="Напишите свой коментарий"></textarea></div>
                        <div class="review_user clearfix">
                            <div class="review_avatar">
                                <img src="catalog/view/theme/default/image/user.png" alt="">
                            </div>
                            <span class="review_name">
                                <?php echo $customer_name; ?>
                            </span>
                            <button type="button" id="button-review" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary pull-right"><?php echo $button_continue; ?></button>
                        </div>
                    </div>
                </div>
                <div class="form-group required review_select_star">
                    <div class="col-sm-12">
                        <label class="control-label"><?php echo $entry_rating; ?></label>
                        <span><?php echo $entry_bad; ?></span>
                        <input type="radio" name="rating" value="1" id="review_star_1"/>
                        <label for="review_star_1"><i class="fa fa-star" aria-hidden="true"></i></label>
                        <input type="radio" name="rating" value="2" id="review_star_2"/>
                        <label for="review_star_2"><i class="fa fa-star" aria-hidden="true"></i></label>
                        <input type="radio" name="rating" value="3" id="review_star_3"/>
                        <label for="review_star_3"><i class="fa fa-star" aria-hidden="true"></i></label>
                        <input type="radio" name="rating" value="4" id="review_star_4"/>
                        <label for="review_star_4"><i class="fa fa-star" aria-hidden="true"></i></label>
                        <input type="radio" name="rating" value="5" id="review_star_5"/>
                        <label for="review_star_5"><i class="fa fa-star" aria-hidden="true"></i></label>
                        <span><?php echo $entry_good; ?></span>
                    </div>
                </div>
                <?php echo $captcha; ?>
            </form>
            <?php } ?>
        </div>
        <div class="feedback">
            <div class="row">
                <div class="col-md-12">
                    <ul class="feedback_list list-unstyled" id="review">
                        
                    </ul>
                </div>
            </div>
        </div>
        <?php if ($column_left || $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-3'; ?>
        <?php } ?>
      <?php echo $content_bottom; ?>
      </div>
    <?php echo $column_right; ?>
    </div>
</div>
</div>
</div>
<script>
    $("#share_show").click(function() {
        $(".share_block").show();
    });
    $(document).click(function(e) {
        if (!$(e.target).closest("#share_show").length) {
            $('.share_block').hide();
            console.log("log");
        }
    e.stopPropagation();
    });
    $(".review_select_star .fa-star").click(function() {
        $(".review_select_star .fa-star").css("color", "#333");
        $(this).css("color", "#ffd800");
        console.log($(this).parent("label").prevAll("label").length);
        $(this).parent("label").prevAll("label").children(".fa-star").css("color", "#ffd800");
    });
</script>
<script type="text/javascript"><!--
$('select[name=\'recurring_id\'], input[name="quantity"]').change(function(){
	$.ajax({
		url: 'index.php?route=product/product/getRecurringDescription',
		type: 'post',
		data: $('input[name=\'product_id\'], input[name=\'quantity\'], select[name=\'recurring_id\']'),
		dataType: 'json',
		beforeSend: function() {
			$('#recurring-description').html('');
		},
		success: function(json) {
			$('.alert, .text-danger').remove();

			if (json['success']) {
				$('#recurring-description').html(json['success']);
			}
		}
	});
});
//--></script>
<script type="text/javascript"><!--
$('#button-cart').on('click', function() {
	$.ajax({
		url: 'index.php?route=checkout/cart/add',
		type: 'post',
		data: $('#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea'),
		dataType: 'json',
		beforeSend: function() {
			$('#button-cart').button('loading');
		},
		complete: function() {
			$('#button-cart').button('reset');
		},
		success: function(json) {
			$('.alert, .text-danger').remove();
			$('.form-group').removeClass('has-error');
			if (json['error']) {
				if (json['error']['option']) {
					for (i in json['error']['option']) {
						var element = $('#input-option' + i.replace('_', '-'));
                        
						if (element.parent().hasClass('input-group')) {
							element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
						} else {
							element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
						}
					}
				}

				if (json['error']['recurring']) {
					$('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
				}

				// Highlight any found errors
				$('.text-danger').parent().addClass('has-error');
			}

			if (json['success']) {
				$("#modal_succes_cart").modal("show");

				$('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');

				$('html, body').animate({ scrollTop: 0 }, 'slow');

				$('#cart > ul').load('index.php?route=common/cart/info ul li');
			}
		},
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
	});
});
//--></script>
<script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});

$('.datetime').datetimepicker({
	pickDate: true,
	pickTime: true
});

$('.time').datetimepicker({
	pickDate: false
});

$('button[id^=\'button-upload\']').on('click', function() {
	var node = this;

	$('#form-upload').remove();

	$('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

	$('#form-upload input[name=\'file\']').trigger('click');

	if (typeof timer != 'undefined') {
    	clearInterval(timer);
	}

	timer = setInterval(function() {
		if ($('#form-upload input[name=\'file\']').val() != '') {
			clearInterval(timer);

			$.ajax({
				url: 'index.php?route=tool/upload',
				type: 'post',
				dataType: 'json',
				data: new FormData($('#form-upload')[0]),
				cache: false,
				contentType: false,
				processData: false,
				beforeSend: function() {
					$(node).button('loading');
				},
				complete: function() {
					$(node).button('reset');
				},
				success: function(json) {
					$('.text-danger').remove();

					if (json['error']) {
						$(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
					}

					if (json['success']) {
						alert(json['success']);

						$(node).parent().find('input').val(json['code']);
					}
				},
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});
		}
	}, 500);
});
//--></script>
<script type="text/javascript"><!--
$('#review').delegate('.pagination a', 'click', function(e) {
    e.preventDefault();

    $('#review').fadeOut('slow');

    $('#review').load(this.href);

    $('#review').fadeIn('slow');
});

$('#review').load('index.php?route=product/product/review&product_id=<?php echo $product_id; ?>');

$('#button-review').on('click', function() {
	$.ajax({
		url: 'index.php?route=product/product/write&product_id=<?php echo $product_id; ?>',
		type: 'post',
		dataType: 'json',
		data: $("#form-review").serialize(),
		beforeSend: function() {
			$('#button-review').button('loading');
		},
		complete: function() {
			$('#button-review').button('reset');
		},
		success: function(json) {
			$('.alert-success, .alert-danger').remove();

			if (json['error']) {
				$('#form-review').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
			}

			if (json['success']) {
				$('#form-review').after('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');

				$('input[name=\'name\']').val('');
				$('textarea[name=\'text\']').val('');
				$('input[name=\'rating\']:checked').prop('checked', false);
			}
		}
	});
});

$(document).ready(function() {
	$('.thumbnails').magnificPopup({
		type:'image',
		delegate: 'a',
		gallery: {
			enabled:true
		}
	});
});
//--></script>
<?php echo $footer; ?>
