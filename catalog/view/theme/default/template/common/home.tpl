<?php echo $header; ?>
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="slider-wrapper hidden-xs" id="slick-1">
                <div class="slider">
                    <a href="/books"><div class="slide" style="background-image:url(catalog/view/theme/default/image/carousel_img1.png); background-repeat: no-repeat; background-position: center right;">
                    </div></a>
                </div>
                <div class="slider-progress">
                    <div class="progress"></div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="home_blocks">
                <div class="home_blocks__title">Ближайшие мероприятия</div>
                <ul class="list-unstyled events_list_home">
                <?php foreach ($events as $events_item) { ?>
                    <li class="clearfix">
                        <a href="<?php echo $events_item['href']; ?>">
                            <?php if($events_item['thumb']) { ?>
                            <div class="events_pic" style="background-image: url('<?php echo $events_item['thumb']; ?>');">
                            </div>
                            <?php }?>
                            <span class="events_title"><?php echo $events_item['title']; ?></span>
                            <span class="events_date"><?php echo $events_item['date']; ?></span>
                        </a>
                    </li> 
                <?php } ?>
                </ul>
                <a href="/events" id="events_all"><i class="fa fa-reply-all" aria-hidden="true"></i>Посмотреть все</a>
            </div>
            <a href="#" class="home_blocks">
                <img src="catalog/view/theme/default/image/combine.png" alt="">Игротека
            </a>
        </div>
    </div>  
</div>
<div class="container">
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?><?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<div class="news_block">
    <div class="container">
        <div class="news_block_head">
            <div class="row">
                <div class="col-md-12">
                    <span class="news_block_title pull-left">Последние новости</span>
                    <a href="/news" class="pull-right all_news">Все новости</a>
                </div>
            </div>    
        </div>
        <div class="row">
            <div class="col-md-12">
                <ul class="list-unstyled news_block_list">
				<?php foreach ($news_list as $news_item) { ?>
                    <li>
                        <a href="<?php echo $news_item['href']; ?>">
                            <?php if($news_item['thumb']) { ?>
                            <span class="news_block_pic">
                                <img src="<?php echo $news_item['thumb']; ?>" alt="<?php echo $news_item['title']; ?>" title="<?php echo $news_item['title']; ?>">
                            </span>
                            <?php }?>
                            <div class="news_block_list_desc">
                                <div class="news_block_list_date">
                                    <i class="fa fa-clock-o" aria-hidden="true"></i>
                                    <span><?php echo $news_item['posted']; ?></span>
                                </div>
                                <div class="news_block_list_title">
                                    <?php echo $news_item['title']; ?>
                                </div>
                                  <div class="news_block_list_text">
                                    <?php echo $news_item['description']; ?>
                                </div>
                            </div>
                        </a>
                    </li> 
				<?php } ?>
				</ul>
            </div>
        </div>
    </div>
</div>
<div class="social_home_wrap">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <span class="map_block_title">Мы в социальных сетях</span>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <script>
                !function (d, id, did, st) {
                  var js = d.createElement("script");
                  js.src = "https://connect.ok.ru/connect.js";
                  js.onload = js.onreadystatechange = function () {
                  if (!this.readyState || this.readyState == "loaded" || this.readyState == "complete") {
                    if (!this.executed) {
                      this.executed = true;
                      setTimeout(function () {
                        OK.CONNECT.insertGroupWidget(id,did,st);
                      }, 0);
                    }
                  }}
                  d.documentElement.appendChild(js);
                }(document,"ok_group_widget","53591618158773",'{"width":250,"height":285}');
                </script>
                <div id="ok_group_widget"></div>
                <script type="text/javascript" src="//vk.com/js/api/openapi.js?146"></script>
                <!-- VK Widget -->
                <div id="vk_groups"></div>
                <script type="text/javascript">
                VK.Widgets.Group("vk_groups", {mode: 3, no_cover: 1, width: "290", color3: '0080CB'}, 33375717);
                </script>
                <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fdomknigi.inform&tabs&width=290&height=203&small_header=true&adapt_container_width=false&hide_cover=true&show_facepile=true&appId" width="290" height="203" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
            </div>
        </div>
    </div>
</div>
<?php echo $footer; ?>