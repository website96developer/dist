<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content= "<?php echo $keywords; ?>" />
<?php } ?>
<script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
<script src="catalog/view/theme/default/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="catalog/view/theme/default/js/function.js" type="text/javascript"></script>
<script src="catalog/view/theme/default/js/jquery.bxslider.js" type="text/javascript"></script>
<script src="catalog/view/theme/default/js/owl.carousel.min.js" type="text/javascript"></script>
<script src="catalog/view/theme/default/js/slick.js" type="text/javascript"></script>
<script src="catalog/view/javascript/jquery/datetimepicker/moment.js"></script>
<script src="catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script src="catalog/view/theme/default/js/jquery.ui.widget.js" type="text/javascript"></script>
<script src="catalog/view/theme/default/js/jquery.fileupload.js" type="text/javascript"></script>
<script src="catalog/view/theme/default/js/jquery.redirect.js" type="text/javascript"></script>
<link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="//fonts.googleapis.com/css?family=Open+Sans:400,400i,300,700" rel="stylesheet" type="text/css" />
<link href="catalog/view/theme/default/stylesheet/stylesheet.css" rel="stylesheet">
<?php foreach ($styles as $style) { ?>
<link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>
<script src="catalog/view/javascript/common.js" type="text/javascript"></script>
<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<?php foreach ($scripts as $script) { ?>
<script src="<?php echo $script; ?>" type="text/javascript"></script>
<?php } ?>
<?php foreach ($analytics as $analytic) { ?>
<?php echo $analytic; ?>
<?php } ?>
</head>
<body class="<?php echo $class; ?>">
<header class="header_small">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div id="logo" class="pull-left">
                    <a href="">
                        <img src="catalog/view/theme/default/image/logo.png">
                    </a>
                </div>
                <nav id="menu" class="navbar navbar-default" role="navigation">
                    <div class="">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>    
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
                            <ul class="nav navbar-nav">
                                <li><a href="/books">Каталог</a></li>
                                <li><a href="/about">О нас</a></li>
                                <li><a href="/news">Новости</a></li>
                                <li><a href="/services">Услуги</a></li>
                                <li><a href="/diskontnayasistema">Дисконтная система</a></li>
                                <li><a href="/events"><img src="catalog/view/theme/default/image/icon_menu.png">Мероприятия и акции</a></li>
                            </ul>
                        </div>
                    </div>
                </nav>   
                <div class="header_small_cart pull-right">
                    <?php echo $cart; ?>
                </div>
            </div>  
        </div>
    </div>
</header>
<nav id="top">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-right">
                <ul class="top_links_list list-unstyled list-inline">
                    <li class="top_links_item">
                        <span class="top_telefone"><?php echo $telephone; ?></span>
                    </li>
                    <li class="top_links_item">
                         <a href="<?php echo $contact; ?>" class="color_gray"><i class="fa fa-paper-plane-o icon_left" aria-hidden="true"></i>Напишите нам</a>
                    </li>
                    <li class="top_links_item">
                        <a href="/contacts" class="color_gray"><i class="fa fa-address-book-o icon_left" aria-hidden="true"></i>Адреса магазинов</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>
<header class="header_big">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-3">
                <div id="logo">
                    <a href="">
                        <img src="catalog/view/theme/default/image/logo.png">
                    </a>
                    <span>НЕ ОСТАНАВЛИВАЙТЕСЬ НА ПРОЧИТАННОМ</span>
                </div>
            </div>
            <div class="col-md-5">
                <div class="search_top_wrapper">
                    <?php echo $search; ?>
                </div>
            </div>
            <div class="col-md-4">
                <div class="cart_top_wrapper">
                    <?php echo $cart; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="header_menu_big">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <nav id="menu" class="navbar navbar-default" role="navigation">
                        <div class="">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>    
                            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                <ul class="nav navbar-nav">
                                    <li id="menu_catalog"><a href="/books">Каталог</a></li>
                                    <li><a href="/about">О нас</a></li>
                                    <li><a href="/news">Новости</a></li>
                                    <li><a href="/services">Услуги</a></li>
                                    <li><a href="/diskontnayasistema">Дисконтная система</a></li>
                                    <li><a href="/events"><img src="catalog/view/theme/default/image/icon_menu.png">Мероприятия и акции</a></li>
                                </ul>
                            </div>
                        </div>
                    </nav>   
                </div>
            </div>
        </div>
    </div>
</header>
<div class="main_menu_dropmenu">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="main_menu_drop_list list-unstyled">
                    <li class="main_menu_drop_item"></li>
                    <?php foreach ($categories as $category) { ?>
                    <?php if ($category['children']) { ?>
                    <li class="main_menu_drop_item">
                        <a href="<?php echo $category['href']; ?>" class="main_menu_main_link"><?php echo $category['name']; ?></a>
                        <ul class="list-unstyled main_menu_dropmenu_right">
                           <?php foreach ($category['children'] as $child) { ?>
                            <li class="drop_item_list_item">
                                <a href="<?php echo $child['href']; ?>" class="drom_item_list_item_mainlink"><?php echo $child['name']; ?></a>
                                <?php if ($child['subchildren']) { ?>
                                    <ul class="drop_item_list_item_drop list-unstyled">
                                    <?php foreach ($child['subchildren'] as $child2) { ?>
                                    <li class="drop_item_list_item_drop_item"><a href="<?php echo $child2['href']; ?>"><?php echo $child2['name']; ?></a></li>
                                    <?php } ?>
                                    </ul>
                               <?php } ?>
                            </li>
                           <?php } ?>
                           <a href="<?php echo $category['href']; ?>" class="main_menu_link_showall">Показать все книги</a>
                        </ul>
                    </li>
                    <?php } else {?>
                    <li class="main_menu_drop_item">
                        <a href="<?php echo $category['href']; ?>" class="main_menu_main_link"><?php echo $category['name']; ?></a>
                    </li>
                    <?php } ?>
                    <?php } ?>
                    <li class="main_menu_drop_item"></li>
                </ul> 
            </div>
        </div>
    </div>
</div>
<script>
    $(".main_menu_drop_item").hover(function(event) {
        $(this).siblings().removeClass("main_menu_drop_item_active");
        $(this).toggleClass("main_menu_drop_item_active");
    });
    $("#menu_catalog").mouseover(function() {
        $(".main_menu_dropmenu").show();
        $(this).addClass("main_menu_select");
        $(this).addClass("menu_catalog_active");
    });
    $(".main_menu_drop_list").mouseleave(function() {
        $(".main_menu_dropmenu").hide();
        $("#menu_catalog").removeClass("menu_catalog_active");
    });
</script>

<!-- Modal succes add product  in cart-->
<div class="modal fade bs-example-modal-lg" id="modal_succes_cart" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal_center">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <div class="succec_circle">
                 <i class="fa fa-check" aria-hidden="true"></i>
                </div>
                <h4 class="modal-title" id="myModalLabel">Товар добавлен в корзину</h4>
            </div>
            <div class="modal-footer">
                <a href="/cart" class="btn_big_blue" >Перейти к оформлению заказа</a>
                <span class="or_margin">или</span>
                <a href="#" class="btn_big_gray" data-dismiss="modal">Продолжить покупки</a>
            </div>
        </div>
    </div>
</div>


<!-- Modal succes add product in wishlist-->
<div class="modal fade bs-example-modal-lg" id="modal_succes_wishlist" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal_center">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <div class="succec_circle">
                 <i class="fa fa-check" aria-hidden="true"></i>
                </div>
                <h4 class="modal-title" id="myModalLabel">Товар добавлен в закладки</h4>
            </div>
            <div class="modal-footer">
                <a href="/wishlist" class="btn_big_blue" >Перейти в закладки</a>
                <span class="or_margin">или</span>
                <a href="#" class="btn_big_gray" data-dismiss="modal">Продолжить покупки</a>
            </div>
        </div>
    </div>
</div>