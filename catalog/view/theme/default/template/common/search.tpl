<div id="search" class="search_main">
    <input type="text" name="search" value="<?php echo $search; ?>" placeholder="<?php echo $text_search; ?>" class="form-control input-lg" />
    <span class="search_select"><span class="search_select_text">Искать везде</span>
        <div class="search_select_box">
            <ul class="search_select_list list-unstyled">
                <li class="search_select_item">
                    <span>Искать везде</span>
                </li>
                <li class="search_select_item">
                    <span>Искать по книгам</span>
                </li>
                <li class="search_select_item">
                    <span>Искать по товарам</span>
                </li>
            </ul>
        </div>
    </span>
    <button type="button" class="btn btn-default btn-lg"></button>
</div>