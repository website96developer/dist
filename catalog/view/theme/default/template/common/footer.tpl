<footer>
  <div class="container">
    <div class="row">
      <div class="col-sm-3">
        <ul class="list-unstyled">
            <li><a href="/books">Каталог</a></li>
            <li><a href="/about">О нас</a></li>
            <li><a href="/services">Услуги</a></li>
            <li><a href="/diskontnayasistema">Дисконтная система</a></li>
        </ul>
      </div>
      <div class="col-sm-3">
        <ul class="list-unstyled">
            <li><a href="/events">Мероприятия и акции</a></li>
            <li><a href="/news">Новости</a></li>
            <li><a href="/parthnersfw">Партнерство</a></li>
            <li><a href="#">Подарки к праздникам</a></li>
        </ul>
      </div>
      <div class="col-sm-3">
        <ul class="list-unstyled">
            <li><a href="/contacts">Контакты</a></li>
            <li><a href="/wparthners">Партнеры</a></li>
            <li><a href="/deliverypay">Оплата и доставка</a></li>
            <li><a href="/contact">Поддержка</a></li>
        </ul>
      </div>
      <div class="col-sm-3">
        <ul class="list-unstyled footer_right">
            <li>
                <span>По всем вопросам звоните</span>
                <span><?php echo $telephone; ?></span>
            </li>
            <li class="social_footer_list">
                <span>Дом Книги в социальных сетях</span>
                <a href="https://vk.com/club33375717" target="_blank"><i class="fa fa-vk" aria-hidden="true"></i></a>
                <a href="https://www.facebook.com/domknigi.inform" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                <a href="https://ok.ru/domknigiekb/topics" target="_blank"><i class="fa fa-odnoklassniki" aria-hidden="true"></i></a>
                <a href="https://www.instagram.com/dom.knigi.ekb/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a>
            </li>
        </ul>
      </div>
    </div>
    <!-- <p><?php echo $powered; ?></p> -->
  </div>
  <div class="footer_bootom">
      <div class="container">
        <div class="row">
            <div class="col-md-8">
                <span>
                    &copy; 2017 Интернет-магазин "Дом Книги"
                </span>
                <a href="/privacy" target="_blank" class="color_blue"><span>Пользовательское соглашение</span></a>
            </div>
            <div class="col-md-4 text-right">
                <a href="#"><img src="catalog/view/theme/default/image/icon_pay.png"></a>
            </div>
        </div>
      </div>
  </div>
</footer>

<!--
OpenCart is open source software and you are free to remove the powered by OpenCart if you want, but its generally accepted practise to make a small donation.
Please donate via PayPal to donate@opencart.com
//-->

<!-- Theme created by Welford Media for OpenCart 2.0 www.welfordmedia.co.uk -->

</body></html>