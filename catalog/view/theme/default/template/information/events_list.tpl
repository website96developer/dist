<?php echo $header; ?>
<div class="container">
	<ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a><i class="fa fa-long-arrow-right" aria-hidden="true"></i></li>
        <?php } ?>
    </ul>
	<div class="row page"><?php echo $column_left; ?>
		<?php if ($column_left && $column_right) { ?>
		<?php $class = 'col-sm-6'; ?>
		<?php } elseif ($column_left || $column_right) { ?>
		<?php $class = 'col-sm-9'; ?>
		<?php } else { ?>
		<?php $class = 'col-sm-12'; ?>
		<?php } ?>
        <div class="col-md-12">
            <h1><?php echo $heading_title; ?></h1>
            <div class="events_filter">
                <span>Сортировать по дате проведения с:</span>
                <div class="dattime_wrap">
                    <div class="input-group datetime">
                        <input type="text" id="date1" data-date-format="YYYY-MM-DD" class="form-control" />
                    </div>
                </div>
                <span>по:</span>
                <div class="dattime_wrap">
                    <div class="input-group datetime">
                        <input type="text" id="date2" data-date-format="YYYY-MM-DD" class="form-control" />
                    </div>
                </div>
                <button class="form-control" id="reset_date">Сбросить</button>
            </div>
        </div>
        <div class="content_wrap">
		<div id="content" class="<?php echo $class; ?> news_page"><?php echo $content_top; ?>
			<?php if ($events_list) { ?>
			<div class="row">
			    <div class="col-md-12 page_news">
                    <ul class="events_list list-unstyled">
                        <?php foreach ($events_list as $events_item) { ?>
                        <li class="clearfix">
                            <a href="<?php echo $events_item['href']; ?>">
                                <?php if($events_item['thumb']) { ?>
                                <span class="news_block_pic">
                                    <img src="<?php echo $events_item['thumb']; ?>" alt="<?php echo $events_item['title']; ?>" title="<?php echo $events_item['title']; ?>">
                                </span>
                                <?php }?>
                                <div class="news_block_list_desc">
                                    <div class="news_block_list_date">
                                        <i class="fa fa-clock-o" aria-hidden="true"></i>
                                        <span><?php echo $events_item['date']; ?></span>
                                    </div>
                                    <div class="news_block_list_title">
                                        <?php echo $events_item['title']; ?>
                                    </div>
                                      <div class="news_block_list_text">
                                        <?php echo $events_item['description']; ?>
                                    </div>
                                </div>
                            </a>
                        </li> 
                        <?php } ?>
                    </ul>
				</div>
			</div>
			<?php if (!empty($pagination)) { ?>
			<div class="row">
                <div class="col-md-12 text-center pagination_wrapper">
                    <?php echo $pagination; ?>
                </div>
            </div>
            <?php } ?>
			<?php } else { ?>
			<p><?php echo $text_empty; ?></p>
			<div class="buttons">
				<div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
			</div>
			<?php } ?>
		<?php echo $content_bottom; ?></div>
		</div>
	<?php echo $column_right; ?></div>
</div>
<script>
    $(document).ready(function() {
        moment.locale('uz');
    });
    $("#date1, #date2").datetimepicker({
        useCurrent: false
    });
    $("#reset_date").click(function() {
        $("#date1").data("DateTimePicker").clear();
        $("#date2").data("DateTimePicker").clear();
        $.ajax({
            type: "get",
            url: "",
            success: function() {
                $act = "http://booksoc/events" + " #content";
                $(".content_wrap").load($act);
            }
        });
    });
    $('.datetime').datetimepicker({
        defaultDate: ''
    }).on('dp.hide', function(e) {
        events_load();
    });
    function events_load() {
        var date1 = $("#date1").val();
        var date2 = $("#date2").val();
        $.ajax({
            type: "get",
            url: "",
            success: function() {
                $act = "http://booksoc/events" + "&date1=" + date1  + "&date2=" + date2  +" #content";
                $(".content_wrap").load($act);
            }
        });
    }
</script>
<?php echo $footer; ?>