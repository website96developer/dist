<?php echo $header; ?>
<div class="container">
	<ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a><i class="fa fa-long-arrow-right" aria-hidden="true"></i></li>
        <?php } ?>
    </ul>
	<div class="row page"><?php echo $column_left; ?>
		<?php if ($column_left && $column_right) { ?>
		<?php $class = 'col-sm-6'; ?>
		<?php } elseif ($column_left || $column_right) { ?>
		<?php $class = 'col-sm-9'; ?>
		<?php } else { ?>
		<?php $class = 'col-sm-12'; ?>
		<?php } ?>
		<div id="content" class="<?php echo $class; ?> services_page"><?php echo $content_top; ?>
			<h1><?php echo $heading_title; ?></h1>
			<?php if ($services_list) { ?>
			<div class="row">
			    <div class="col-md-12">
                    <div class="services_wrapper">
                        <ul class="services_list list-unstyled list-inline">
                            <?php foreach ($services_list as $services_item) { ?>
                            <li class="services_item">
                                <a href="<?php echo $services_item['href']; ?>">
                                    <?php if($services_item['thumb']) { ?>
                                    <span class="services_block_pic">
                                        <img src="<?php echo $services_item['thumb']; ?>" alt="<?php echo $services_item['title']; ?>" title="<?php echo $services_item['title']; ?>">
                                    </span>
                                    <?php }?>
                                    <span class="services_item_title"><span><?php echo $services_item['title']; ?></span></span>
                                </a>
                            </li>
                            <?php } ?>
                        </ul>
                    </div>
				</div>
			</div>
			<?php if (!empty($pagination)) { ?>
			<div class="row">
                <div class="col-md-12 text-center pagination_wrapper">
                    <?php echo $pagination; ?>
                </div>
            </div>
            <?php } ?>
			<?php } else { ?>
			<p><?php echo $text_empty; ?></p>
			<div class="buttons">
				<div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
			</div>
			<?php } ?>
		<?php echo $content_bottom; ?></div>
	<?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>