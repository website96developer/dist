<?php echo $header; ?>
<div class="container">
    <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a><i class="fa fa-long-arrow-right" aria-hidden="true"></i></li>
        <?php } ?>
    </ul>
    <div class="row page">
        <?php if ( $hrefs == "http://booksoc/contacts") {  ?>
        <div class="col-md-12">
            <h1><?php echo $heading_title; ?></h1>
            <div class="contact_wrapper clearfix">
                <div class="contacts_left">
                    <h3>Сеть магазинов &laquo;Дом книги&raquo;</h3>
                    <ul class="list-unstyled contacts_list">
                        <li class="contacts_item">
                            <span>+7 (343) 289-40-45</span>
                            <a href="mailto:domkniginform@mail.ru" target="_blank">domkniginform@mail.ru</a>
                        </li>
                        <li class="contacts_item">
                            <span>
                                620014, г.Екатеринбург ул. Антона Валека 12
                            </span>
                        </li>
                        <li class="contacts_item">
                            <span>
                                <img src="catalog/view/theme/default/image/contacts/whatsapp.png" alt="">+ 7 (922) 11-66-821
                            </span>
                        </li>
                    </ul>
                    <div class="contacts_link">
                        <a href="/contact"><i class="fa fa-headphones" aria-hidden="true"></i>Поддержка клиентов</a>
                        <a href="#" data-toggle="modal" data-target="#modal_requisites"><i class="fa fa-briefcase" aria-hidden="true"></i>Реквизиты компании</a>
                    </div>
                </div>
                <div class="contacts_right">
                    <div class="contacts_right_map"><a class="dg-widget-link" href="http://2gis.ru/ekaterinburg/profiles/1267165676585525,1267165676585527,1267165676585529,70000001025847295,1267165676585526,1267166676311876,70000001007009630,1267165676585528,1267165676739735,1267165676585530,1267165676637267/center/60.660324096679695,56.89400373250571/zoom/11?utm_medium=widget-source&utm_campaign=firmsonmap&utm_source=bigMap">Посмотреть на карте Екатеринбурга</a><script charset="utf-8" src="http://widgets.2gis.com/js/DGWidgetLoader.js"></script><script charset="utf-8">new DGWidgetLoader({"width":812,"height":678,"borderColor":"#a3a3a3","pos":{"lat":56.89400373250571,"lon":60.660324096679695,"zoom":11},"opt":{"city":"ekaterinburg"},"org":[{"id":"1267165676585525"},{"id":"1267165676585527"},{"id":"1267165676585529"},{"id":"70000001025847295"},{"id":"1267165676585526"},{"id":"1267166676311876"},{"id":"70000001007009630"},{"id":"1267165676585528"},{"id":"1267165676739735"},{"id":"1267165676585530"},{"id":"1267165676637267"}]});</script><noscript style="color:#c00;font-size:16px;font-weight:bold;">Виджет карты использует JavaScript. Включите его в настройках вашего браузера.</noscript></div>
                    <img src="catalog/view/theme/default/image/img1.png" class="contacts_right_foto">
                    <div class="map_button_wrapper">
                        <span class="map_button_map">На карте</span>
                        <span class="map_button_foto">Фото</span>
                    </div>
                </div>
            </div>
            <div class="contacts_table">
                <div class="contacts_store_head">
                    <span class="store_title">
                        Точка продаж в
                    </span>
                    <span class="store_select">
                        <span id="city_store">Екатеринбург</span>
                        <div class="store_select_wrap">
                            <ul class="store_select_list list-unstyled">
                                <li class="store_select_item">
                                    <span data-city="ekat" data-count="9">Екатеринбург</span>
                                </li>
                                <li class="store_select_item">
                                    <span data-city="pushma" data-count="1">Верхняя Пышма</span>
                                </li>
                                <li class="store_select_item">
                                    <span data-city="berez" data-count="1">Березовский</span>
                                </li>
                            </ul>
                        </div>
                    </span>
                    <span class="count_store">
                        <span>Количество магазинов:</span>
                        <span class="count_store_num">9</span>
                    </span>
                </div>
                <table id="ekat" class="table_city">
                    <thead>
                        <tr>
                            <td>Адрес</td>
                            <td>Телефон</td>
                            <td>Время работы</td>
                            <td></td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td data-label="Адрес">ул. Антона Валека, 12</td>
                            <td data-label="Телефон">+7 343 289-40-45</td>
                            <td data-label="Время работы">ПН-ВС: 10:00 - 20:00</td>
                            <td><a href="#">Показать на карте</a></td>
                        </tr>
                        <tr>
                            <td data-label="Адрес">ул. Баумана, 3</td>
                            <td data-label="Телефон">+7 343 331-14-61</td>
                            <td data-label="Время работы">ПН-СБ: 10:00 - 20:00; ВС: 10:00 - 19:00</td>
                            <td><a href="#">Показать на карте</a></td>
                        </tr>
                        <tr>
                            <td data-label="Адрес"> ул. Заводская, 17</td>
                            <td data-label="Телефон">205-15-30</td>
                            <td data-label="Время работы">ПН-СБ: 10:00 - 19:00; ВС 10:00 - 19:00</td>
                            <td><a href="#">Показать на карте</a></td>
                        </tr>
                        <tr>
                            <td data-label="Адрес">ул. Опалихинская, 15</td>
                            <td data-label="Телефон">+7 343 245-32-33</td>
                            <td data-label="Время работы">ПН-ПТ: 10:00 - 20:00; СБ-ВС: 10:00 - 19:00</td>
                            <td><a href="#">Показать на карте</a></td>
                        </tr>
                        <tr>
                            <td data-label="Адрес">ул. Техническая, 63, ТЦ "7 ключей" (1 этаж)</td>
                            <td data-label="Телефон">+7 343 366-94-66</td>
                            <td data-label="Время работы">ПН-СБ: 10:00 - 20:00; ВС: 10:00 - 19:00</td>
                            <td><a href="#">Показать на карте</a></td>
                        </tr>
                        <tr>
                            <td data-label="Адрес">ул. 8 марта, 149, ТРЦ "Мегаполис" (4 этаж)</td>
                            <td data-label="Телефон">+7 343 385-05-37</td>
                            <td data-label="Время работы">ПН-ВС: 10:00 - 22:00</td>
                            <td><a href="#">Показать на карте</a></td>
                        </tr>
                        <tr>
                            <td data-label="Адрес">ул. Вильгельма де Геннина, 33</td>
                            <td data-label="Телефон">+7 343 366-78-22</td>
                            <td data-label="Время работы">ПН-ПТ: 10:00 - 20:00; СБ-ВС: 10:00 - 19:00</td>
                            <td><a href="#">Показать на карте</a></td>
                        </tr>
                        <tr>
                            <td data-label="Адрес">ул.Викулова, 33/1</td>
                            <td data-label="Телефон">+7 343 203-30-04</td>
                            <td data-label="Время работы">ПН-ВС: 10:00 - 19:00</td>
                            <td><a href="#">Показать на карте</a></td>
                        </tr>
                        <tr>
                            <td data-label="Адрес">ул. Сыромолотова,22</td>
                            <td data-label="Телефон">+7 343 207-40-45</td>
                            <td data-label="Время работы">ПН-ВС: 10:00 - 22:00</td>
                            <td><a href="#">Показать на карте</a></td>
                        </tr>
                    </tbody>
                </table>
                <table id="berez" class="table_city other_city">
                    <thead>
                        <tr>
                            <td>Адрес</td>
                            <td>Телефон</td>
                            <td>Время работы</td>
                            <td></td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td data-label="Адрес">ул. Красных героев, 3</td>
                            <td data-label="Телефон">+7 34369 4-31-56</td>
                            <td data-label="Время работы">ПН-ВС: 10:00 - 20:00</td>
                            <td><a href="#">Показать на карте</a></td>
                        </tr>
                    </tbody>
                </table>
                <table id="pushma" class="table_city other_city">
                    <thead>
                        <tr>
                            <td>Адрес</td>
                            <td>Телефон</td>
                            <td>Время работы</td>
                            <td></td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td data-label="Адрес">ул. Уральских рабочих, 49</td>
                            <td data-label="Телефон">+7 34368 5-86-66</td>
                            <td data-label="Время работы">ПН-ВС: 10:00 - 20:00</td>
                            <td><a href="#">Показать на карте</a></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="modal fade" id="modal_requisites" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Реквизиты компании</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="requisiter_text">Общество с ограничнной ответственностью &laquo;Дом книги&raquo; 620014, г. Екатеринбург, ул. Антона Валека 12</div>
                                <div class="requisiter_wrapper">
                                    <ul class="requisiter_list list-unstyled">
                                        <li class="requisites_item clearfix">
                                            <div class="requisites_item_column text-right">
                                                ИНН
                                            </div>
                                            <div class="requisites_item_column text-left">
                                                6658004862
                                            </div>
                                        </li>
                                        <li class="requisites_item clearfix">
                                            <div class="requisites_item_column text-right">
                                                КПП:
                                            </div>
                                            <div class="requisites_item_column text-left">
                                                665801001
                                            </div>
                                        </li>
                                        <li class="requisites_item clearfix">
                                            <div class="requisites_item_column text-right">
                                                ОГРН:
                                            </div>
                                            <div class="requisites_item_column text-left">
                                                1026602332900
                                            </div>
                                        </li>
                                        <li class="requisites_item clearfix">
                                            <div class="requisites_item_column text-right">
                                                ОКПО:
                                            </div>
                                            <div class="requisites_item_column text-left">
                                                25026029
                                            </div>
                                        </li>
                                        <li class="requisites_item clearfix">
                                            <div class="requisites_item_column text-right">
                                                Банк:
                                            </div>
                                            <div class="requisites_item_column text-left">
                                                ОАО «Московский кредитный банк»
                                            </div>
                                        </li>
                                        <li class="requisites_item clearfix">
                                            <div class="requisites_item_column text-right">
                                                Бик:
                                            </div>
                                            <div class="requisites_item_column text-left">
                                                044585659
                                            </div>
                                        </li>
                                        <li class="requisites_item clearfix">
                                            <div class="requisites_item_column text-right">
                                                Расчетный счет:
                                            </div>
                                            <div class="requisites_item_column text-left">
                                                407028103015123812803
                                            </div>
                                        </li>
                                        <li class="requisites_item clearfix">
                                            <div class="requisites_item_column text-right">
                                                Корреспондетский счет:
                                            </div>
                                            <div class="requisites_item_column text-left">
                                                301018103000001951858
                                            </div>
                                        </li>
                                    </ul>       
                                </div>
                                <div class="requisites_link">
                                    <a href="/cardfirm.pdf" download>
                                        <span class="requisites_link_icon">
                                            pdf
                                        </span>
                                        <span class="requisites_link_text">
                                            Скачать реквизиты компании в PDF
                                        </span>
                                    </a>
                                    <a href="/cardfirm.docx" download class="pull-right">
                                        <span class="requisites_link_icon">
                                            txt
                                        </span>
                                        <span class="requisites_link_text">
                                            Скачать реквизиты компании в TXT
                                        </span>
                                    </a>
                                </div>    
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php } else {?>
        <div id="content" class="col-md-9 col-sm-9">
            <h1><?php echo $heading_title; ?></h1>
            <?php echo $description; ?>
            <?php if ($images) { ?>
            <div class="content_slider_wrapper">
                <div class="content_slider owl-carousel">
                    <?php foreach ($images as $image) { ?>
                    <div class="item">
                        <img src="<?php echo $image['popup']; ?>" alt="">
                    </div>
                    <?php } ?>
                </div>
                <span class="slider_couner__count"></span>
            </div>
            <?php } ?>
            <?php echo $content_bottom; ?>
        </div>
        <?php echo $column_right; ?>
        <div class="col-md-12">
            <div class="share_block_bottom clearfix">
                <span class="share_title">Поделитесь в социальных сетях:</span>
                <ul class="list-unstyled share_list">
                    <li class="share_item">
                        <a target="_blank" href="http://vk.com/share.php?url=<?php echo $hrefs; ?>&title=<?php echo $heading_title; ?>" class="share_link share_vk">Вконтакте</a>
                    </li>
                    <li class="share_item">
                        <a target="_blank" href="http://www.facebook.com/sharer.php?u=<?php echo $hrefs; ?>" class="share_link share_fb">Facebook</a>
                    </li>
                    <li class="share_item">
                        <a target="_blank" href="http://www.ok.ru/dk?st.cmd=addShare&st.s=1&st._surl=<?php echo $hrefs; ?>" class="share_link share_odn">Одноклассники</a>
                    </li>
                </ul>
            </div>
        </div>
        <?php } ?>
    </div>
</div>
<?php echo $footer; ?>