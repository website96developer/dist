<?php echo $header; ?>
<div class="container">
    <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a><i class="fa fa-long-arrow-right" aria-hidden="true"></i></li>
        <?php } ?>
    </ul>
    <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
        <div id="content" class="col-md-8"><?php echo $content_top; ?>
        <h1><?php echo $heading_title; ?></h1>
        <span class="mini_title">Для обращения в центр поддержки клиентов вам необходимо заполнить заявку ниже:</span>
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="contact_form">
            <div class="row">
                <div class="col-md-12">
                        <div class="form-group">
                            <label for="name">Выберите категорию обращения</label>
                            <div class="select_custom">
                                <select class="form-control" name="subject-contact">
                                    <option>Проведение мероприятий</option>
                                    <option>Доставка и оплата</option>
                                    <option>Заказ через сайт</option>
                                    <option>Кадровый отдел</option>
                                    <option>IT отдел</option>
                                    <option>Другой вопрос</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="input-enquiry">Опишите ваш вопрос</label>
                            <textarea name="enquiry" rows="10" id="input-enquiry" class="form-control"><?php echo $enquiry; ?></textarea>
                            <?php if ($error_enquiry) { ?>
                            <div class="text-danger"><?php echo $error_enquiry; ?></div>
                            <?php } ?>
                        </div>
                        <div class="form_input_file">
                        <label class="input_file" for="file_contact"><span><i class="fa fa-plus" aria-hidden="true"></i></span>Прикрепить файлы</label>
                        <input type="file" id="file_contact" name="files[]" data-url="catalog/view/theme/default/server/php/" multiple>
                        <ul class="contact_file_list list-unstyled list-inline"></ul>
                        <script>
                            $(function () {
                                $('#file_contact').fileupload({
                                    dataType: 'json',
                                    done: function (e, data) {
                                        $.each(data.result.files, function (index, file) {
                                            $('<li/>').text(file.name).appendTo($(".contact_file_list")).prepend('<div class="delete" dada-url="file.deleteUrl">Delete</div>');
                                            console.log(file);
                                        });
                                    }
                                });
                                $(".delete").click(function() {
                                    var delete_url = $(this).attr("data-url");
                                    console.log(delete_url);
                                    $("#file_contact").bind('fileuploaddestroy', function(e, data) {
                                        url: delete_url
                                    });    
                                });
                            });
//                                $("#file_contact").change(function() {
//                                    var $el = $(this),
//                                        fileName;
//                                    if ($el.val().lastIndexOf('\\')) {
//                                        var i = $el.val().lastIndexOf('\\') + 1;
//                                    } else {
//                                        var i = $el.val().lastIndexOf('/') + 1;
//                                    }
//                                    fileName = $el.val().slice(i);
//                                    $(".contact_file_list").append("<li class='contact_file_item'>" + fileName + "</li>");
//                                });
                        </script>
                       </div>
                </div>
            </div>           
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="input-name">Имя</label>
                        <input type="text" name="name" value="<?php echo $name; ?>" id="input-name" class="form-control">
                        <?php if ($error_name) { ?>
                        <div class="text-danger"><?php echo $error_name; ?></div>
                        <?php } ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="name">Фамилия</label>
                        <input type="text" name="surname" id="input-surname" class="form-control">
                        <?php if ($error_surname) { ?>
                        <div class="text-danger"><?php echo $error_surname; ?></div>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="name">Телефон</label>
                        <input type="text" name="telefone" class="form-control" id="input-telefone">
                        <?php if ($error_telefon) { ?>
                        <div class="text-danger"><?php echo $error_telefon; ?></div>
                        <?php } ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="name">Email</label>
                        <input type="email" name="email" value="<?php echo $email; ?>" id="input-email" class="form-control">
                        <?php if ($error_email) { ?>
                        <div class="text-danger"><?php echo $error_email; ?></div>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="contact_footer">
                        <div class="checkboxFive">
                            <input type="checkbox" value="" id="check_agreement" name="">
                            <label for="check_agreement"></label>
                            <span>Принимаю <a href="#">пользовательское соглашение</a></span>
                        </div>
                        <input class="btn_big_blue" id="submit_contact" type="submit" value="<?php echo $button_submit; ?>" disabled/>
                    </div>
                </div>
            </div>
            <script>
                $("#contact_form .form-group").each(function() {
                    if ($(this).find(".text-danger").is(":visible")) {
                        $(this).find("input, textarea").addClass("no_valid");
                    } else {
                        $(this).find("input, textarea").removeClass("no_valid");
                    }
                });
                $("#check_agreement").click(function() {
                    check_contact();
                });
                function check_contact() {
                    if ( $("#check_agreement").is(":checked") ) {
                        $("#submit_contact").removeAttr("disabled");
                    }     
                    else {
                        $("#submit_contact").attr("disabled", "disabled");
                    }
                }
                $("#input-telefone").mask("+7 999 999-99-99");
//                var input_enquiry,
//                    contact_name,
//                    contact_surname,
//                    contact_tel,
//                    contact_email;
//                function check_contact() {
//                    input_enquiry = $("#input-enquiry").val();
//                    contact_name = $("#input-name").val();
//                    contact_surname = $("#input-surname").val();
//                    contact_tel = $("#input-telefone").val();
//                    contact_email = $("#input-email").val();
//                    if ( input_enquiry.length != 0 && contact_name.length != 0 && contact_surname.length != 0 && contact_email.length != 0 && contact_tel.length != 0 && contact_tel != '8(___) ___-__-__' && contact_tel.indexOf('_') < 0 && $("#check_agreement").is(":checked") ) {
//                        $("#submit_contact").removeAttr("disabled");
//                        $("#submit_contact").removeClass("btn_blue_disabled");
//                    }     
//                    else {
//                        $("#submit_contact").attr("disabled", "disabled");
//                        $("#submit_contact").addClass("btn_blue_disabled");
//                    }
//                }
//                $("#input-name", "#input-surname", "#input-telefone", "#input-email", "#input-enquiry").keyup(function() {
//                    check_contact();
//                });
//                $("#check_agreement").click(function() {
//                    check_contact();
//                });
            </script>
        </form>
        <?php echo $content_bottom; ?>
        </div>
    <?php echo $column_right; ?>
    </div>
</div>
<?php echo $footer; ?>