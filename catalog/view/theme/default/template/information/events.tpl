<?php echo $header; ?>
<div class="container">
	<ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a><i class="fa fa-long-arrow-right" aria-hidden="true"></i></li>
        <?php } ?>
    </ul>
	<div class="row"><?php echo $column_left; ?>
		<?php if ($column_left && $column_right) { ?>
		<?php $class = 'col-sm-6'; ?>
		<?php } elseif ($column_left || $column_right) { ?>
		<?php $class = 'col-sm-9'; ?>
		<?php } else { ?>
		<?php $class = 'col-sm-12'; ?>
		<?php } ?>
		<div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
			<div class="row">
			    <?php if ($thumb) { ?>
				<div class="col-sm-4">
					<div class="thumbnail">
						<a href="<?php echo $popup; ?>" title="<?php echo $heading_title; ?>"><img src="<?php echo $thumb; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>"/></a>
					</div>
				</div>
				<?php } ?>
				<div class="<?php echo $thumb ? 'col-sm-8' : 'col-sm-12'; ?>">
					<h1><?php echo $heading_title; ?></h1>
					<div class="news_page_desc">
                        <span class="news_page_desc_item">
                            <i class="fa fa-clock-o"></i><?php echo $posted; ?>
                        </span>
					</div>
					<div class="news_page_content">
						<div class="description">
							<?php echo $description; ?>
						</div>
					</div>
				</div>
                <div class="col-md-12">
                    <div class="share_block_bottom clearfix">
                        <span class="share_title">Поделитесь в социальных сетях:</span>
                        <ul class="list-unstyled share_list">
                            <li class="share_item">
                                <a target="_blank" href="http://vk.com/share.php?url=<?php echo $hrefs; ?>&title=<?php echo $heading_title; ?>" class="share_link share_vk">Вконтакте</a>
                            </li>
                            <li class="share_item">
                                <a target="_blank" href="http://www.facebook.com/sharer.php?u=<?php echo $hrefs; ?>" class="share_link share_fb">Facebook</a>
                            </li>
                            <li class="share_item">
                                <a target="_blank" href="http://www.ok.ru/dk?st.cmd=addShare&st.s=1&st._surl=<?php echo $hrefs; ?>" class="share_link share_odn">Одноклассники</a>
                            </li>
                        </ul>
                    </div>
                </div>
			</div>
		<?php echo $content_bottom; ?></div>
	<?php echo $column_right; ?></div>
	<script type="text/javascript"><!--
		$(document).ready(function () {
			$('.thumbnail').magnificPopup({
				type: 'image',
				delegate: 'a',
			});
		});
	//--></script>
</div>
<?php echo $footer; ?>