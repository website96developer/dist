<?php echo $header; ?>
<div class="container">
	<ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a><i class="fa fa-long-arrow-right" aria-hidden="true"></i></li>
        <?php } ?>
    </ul>
	<div class="row page"><?php echo $column_left; ?>
		<?php if ($column_left && $column_right) { ?>
		<?php $class = 'col-sm-6'; ?>
		<?php } elseif ($column_left || $column_right) { ?>
		<?php $class = 'col-sm-9'; ?>
		<?php } else { ?>
		<?php $class = 'col-sm-12'; ?>
		<?php } ?>
		<div id="content" class="<?php echo $class; ?> news_page"><?php echo $content_top; ?>
			<h1><?php echo $heading_title; ?></h1>
			<?php if ($news_list) { ?>
			<div class="row">
			    <div class="col-md-12 page_news">
                    <ul class="list-unstyled news_block_list">
                    <?php foreach ($news_list as $news_item) { ?>
                        <li class="clearfix">
                            <a href="<?php echo $news_item['href']; ?>">
                                <?php if($news_item['thumb']) { ?>
                                <span class="news_block_pic">
                                    <img src="<?php echo $news_item['thumb']; ?>" alt="<?php echo $news_item['title']; ?>" title="<?php echo $news_item['title']; ?>">
                                </span>
                                <?php }?>
                                <div class="news_block_list_desc">
                                    <div class="news_block_list_date">
                                        <i class="fa fa-clock-o" aria-hidden="true"></i>
                                        <span><?php echo $news_item['posted']; ?></span>
                                    </div>
                                    <div class="news_block_list_title">
                                        <?php echo $news_item['title']; ?>
                                    </div>
                                      <div class="news_block_list_text">
                                        <?php echo $news_item['description']; ?>
                                    </div>
                                </div>
                            </a>
                        </li> 
                    <?php } ?>
                    </ul>
				</div>
			</div>
			<?php if (!empty($pagination)) { ?>
			<div class="row">
                <div class="col-md-12 text-center pagination_wrapper">
                    <?php echo $pagination; ?>
                </div>
            </div>
            <?php } ?>
			<?php } else { ?>
			<p><?php echo $text_empty; ?></p>
			<div class="buttons">
				<div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
			</div>
			<?php } ?>
		<?php echo $content_bottom; ?></div>
	<?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>