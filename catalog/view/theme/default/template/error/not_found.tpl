<?php echo $header; ?>
<div class="container">
    <div class="row">
        <?php echo $column_left; ?>
        <?php if ($column_left && $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
        <?php $class = 'col-sm-9'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-12'; ?>
        <?php } ?>
        <div id="content" class="col-md-6 col-md-offset-3 <?php echo $class; ?> text-center page404"><?php echo $content_top; ?>
            <span class="title404">404</span>
            <span class="mini_title404">В доступе отказано</span>
            <p>Проверьте правильность введеного вами адрес,</p><p>воспользуйтесь поиском либо начните <a href="/index">с главной страницы</a></p>
            <?php echo $search; ?>
            <?php echo $content_bottom; ?>
        </div>
        <?php echo $column_right; ?>
    </div>
</div>
<?php echo $footer; ?>