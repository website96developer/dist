<?php echo $header; ?>
<div class="container">
    <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a><i class="fa fa-long-arrow-right" aria-hidden="true"></i></li>
        <?php } ?>
    </ul>
    <?php if ($attention) { ?>
    <div class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo $attention; ?>
        <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
        <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="row"><?php echo $column_left; ?>
        <?php if ($column_left && $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
        <?php $class = 'col-sm-9'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-12'; ?>
        <?php } ?>
    </div>
    <div class="row">
        <div class="col-md-12">
            <span class="category_title"><?php echo $heading_title; ?></span>
        </div>
    </div>
    <div class="row">
        <div class="col-md-9 left_column_cart">
            <div class="table_cart"><?php echo $content_top; ?>
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
                    <div class="table_cart_head">
                        <div class="row">
                            <div class="text-left col-md-6">
                                <div class="table_cart_title">
                                    <?php echo $column_name; ?>
                                </div>
                            </div>
                            <div class="text-center col-md-3">
                                <div class="table_cart_title">
                                    <?php echo $column_quantity; ?>
                                </div>
                            </div>
                            <div class="text-left col-md-3">
                                <div class="table_cart_title">
                                    <?php echo $column_price; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php foreach ($products as $product) { ?>
                    <div class="table_cart_tr">
                        <div class="row">
                            <div class="text-center col-md-2">
                                <?php if ($product['thumb']) { ?>
                                <a href="<?php echo $product['href']; ?>" class="cart_img_link"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-thumbnail" /></a>
                                <?php } ?>
                            </div>
                            <div class="text-left col-md-4">
                                <a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
								<?php foreach($product['attributes'] as $group_attr) {
									if($group_attr['attribute_group_id']==7) {
										foreach($group_attr['attribute'] as $attr) {
											if($attr['attribute_id']==12) {
												?>
												<span class="table_cart_tr_title"><?php echo $attr['name']; ?></span>
												<span class="table_cart_tr_text"><?php echo $attr['text']; ?></span>
												<?php
											}
										}
									}
								} ?>
								<?php foreach($product['option'] as $option) { ?>
									<span class="table_cart_tr_title"><?php echo $option['name']; ?></span>
									<span class="table_cart_tr_text"><?php echo $option['value']; ?></span>											
								<?php } ?>
                            </div>
                            <div class="text-left col-md-3 text-center table_cart_count">
                                <i class="fa fa-plus" aria-hidden="true"></i>
                                <input id="count_product" class="form-control" type="number" min=1 name="quantity[<?php echo $product['cart_id']; ?>]" value="<?php echo $product['quantity']; ?>" data-key="<?php echo $product['cart_id']; ?>">
                                <i class="fa fa-minus" aria-hidden="true"></i>
                                <button id="update_count_product" type="submit" data-toggle="tooltip" title="<?php echo $button_update; ?>" class="btn btn-primary hidden"><i class="fa fa-refresh"></i></button>
                            </div>
                            <div class="text-left col-md-2 table_cart_price">
                                <?php echo $product['price']; ?><i class="fa fa-rub" aria-hidden="true"></i>
                            </div>
                            <div class="text-right col-md-1 table_cart_delete">
                                <a href="#" onclick="cart.remove('<?php echo $product['cart_id']; ?>');">
                                <span class="table_cart_tr_delete"><i class="fa fa-times" aria-hidden="true"></i></span>
                                </a>
                            </div>
                       </div>
                    </div>
                    <?php } ?>
                    <div class="row">
                        <div class="table_cart_footer">
                            <div class="col-md-12 text-right">
                                <span>Стоимость товаров без учета доставки:</span>
                                <span class="table_cart_full_price"><?php echo $totals[1]['text']; ?></span><i class="fa fa-rub" aria-hidden="true"></i>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="cart_block">
                <div class="row">
                    <div class="col-md-6">
                        <span class="title_cart">Личные данные</span>
                    </div>
                    <?php if (!($logged)) { ?>
                    <div class="col-md-6 text-right">
                        <a href="/login" class="cart_text cart_text_blue">Войдите</a>
                        <span class="cart_text">в аккаунт для быстрого заполнения данных</span>
                    </div>
                    <?php } ?>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                        <label for="name">Имя<span class="form_group_star">*</span></label>
                        <input type="text" class="form-control" id="name" value="<?php echo $firstname; ?>">
						<span class="error_submit" id="error_name">Введите корректное значение</span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                        <label for="name">Фамилия<span class="form_group_star">*</span></label>
                        <input type="text" class="form-control" id="surname" value="<?php echo $lastname; ?>">
						<span class="error_submit" id="error_surname">Введите корректное значение</span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="name">Телефон<span class="form_group_star">*</span></label>
                            <input type="text" class="form-control" id="phone" value="<?php echo $telephone; ?>">
							<span class="error_submit" id="error_phone">Введите корректное значение</span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="name">Email<span class="form_group_star">*</span></label>
                            <input type="email" required class="form-control" id="email" value="<?php echo $email; ?>">
							<span class="error_submit" id="error_email">Введите корректное значение</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="cart_block">
                <div class="row">
                    <div class="col-md-6 col-sm-6">
                        <span class="title_cart">Способ доставки</span>
						<span class="error_submit" id="error_delivery">Некорректный способ доставки</span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <ul class="cart_list_buy_data list-unstyled">
                            <li class="cart_block_item">
                                <div class="cart_block_item_left_select">
                                    <div class="cart_block_item_left_wrapper">
                                        <input type="radio" class="radio" id="select_courier" name="select_delivery" onclick="$('#address').show()">
                                        <label class="radio_label" for="select_courier">Доставка курьером</label>    
                                    </div>
                                </div>
                                <div class="cart_block_item_right">
                                    <span>350<i class="fa fa-rub" aria-hidden="true"></i></span>
                                </div>
                                <div class="radio_desc">Доставка курьером нашей компании в любую точку города в будние т выходные дни, с 10.00 до 21.00</div>
                            </li> 
                            <li class="cart_block_item">
                                <div class="cart_block_item_left_select">
                                    <div class="cart_block_item_left_wrapper">
                                        <input type="radio" class="radio" id="select_pickup" name="select_delivery" onclick="$('#address').hide()">
                                        <label class="radio_label" for="select_pickup">Самовывоз</label>                                      
                                    </div>
                                </div>
                                <div class="cart_block_item_right">
                                    <span>Бесплатно</span>
                                </div>
                                <div class="radio_desc">
                                    <a href="#" data-toggle="modal" data-target="#modal_delivery_point" class="select_sity_delivery">Выберите точку самовывоза</a>
                                    <div id="delivery_point"></div>
                                </div>
                            </li>
                        </ul>
                    </div> 
                </div>
            </div>
            <div class="cart_block">  
                <div class="row">
                    <div class="col-md-12">
                        <span class="title_cart">Способ оплаты</span>
						<span class="error_submit" id="error_payment">Некорректный способ оплаты</span>
                        <ul class="cart_list_buy_data list-unstyled">
                            <li class="cart_block_item">
                                <input type="radio" class="radio" id="payment_cash" name="select_payment">
                                <label class="radio_label" for="payment_cash">Наличными</label>    
                            </li>
                            <li class="cart_block_item">
                                <input type="radio" class="radio" id="payment_card" name="select_payment">
                                <label class="radio_label" for="payment_card">Банковской картой курьеру<img src="catalog/view/theme/default/image/cart_oplata.png" alt=""></label>    
                            </li>
                            <li class="cart_block_item">
                                <input type="radio" class="radio" id="payment_electronic" name="select_payment">
                                <label class="radio_label" for="payment_electronic">Электронными деньгами, банковской картой он-лайн<img src="catalog/view/theme/default/image/icon-copy.png" alt=""></label>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="cart_block" id="address" style="display:none">
                <div class="row">
                    <div class="col-md-12">
                        <span class="title_cart">Адрес доставки</span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="name">Улица</label>
                            <input type="text" class="form-control" placeholder="" id="street">
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="name">Дом</label>
                            <input type="text" class="form-control" placeholder="" id="house">
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="name">Корпус</label>
                            <input type="text" class="form-control" placeholder="" id="building">
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="name">Квартира</label>
                            <input type="text" class="form-control" placeholder="" id="apartment">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="cart_box_help">
                        <span>Если у вас возникли вопросы с оформлением заказа, свяжитесь с нами по телефону:</span>
                        <span class="cart_box_help_mobile"><?php echo $telephone; ?></span>
                    </div>
                </div>
            </div>
            <?php echo $content_bottom; ?>
            <?php echo $column_right; ?>
        </div>
        <div class="col-md-3">
            <div class="right_column_cart">
                <span class="right_column_title">Ваш заказ</span>
                <ul class="right_column_list_product list-unstyled">
                    <?php foreach ($products as $product) { ?>
                    <li class="right_column_item_product">
                        <span class="item_product_title"><?php echo $product['name']; ?></span>
                        <div>
                            <span class="item_product_count"><?php echo $product['quantity']; ?></span>
                            <span class="item_product_count">x</span>
                            <span><?php echo $product['total']; ?></span>
                            <i class="fa fa-rub" aria-hidden="true"></i>
                        </div>
                    </li>
                    <?php } ?>
                </ul>
                <div class="right_column_footer_wrapp">
                    <span class="right_column_footer">Итого к оплате</span>
                    <span class="text-right right_column_footer right_column_footer_price"><?php echo $totals[1]['text']; ?><i class="fa fa-rub" aria-hidden="true"></i></span>
                </div>
                <a class="btn_big_blue" id="submit">Оформить заказ</a>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal_delivery_point" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal_border"></div>
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Выберите точку самовывоза</h4>
                <span class="modal_delivery_point_text"></span>
                <button id="delivery_point_succes"class="btn_big_blue" >Верно</button>
            </div>
            <div class="modal-body text-center">
                <ul class="list-unstyled delivery_point_list">
                    <li class="delivery_point_item">
                        г. Екатеринбург, ул. Антона Валека, 12
                    </li>
                    <li class="delivery_point_item">
                        г. Екатеринбург, ул. Баумана, 3
                    </li>
                    <li class="delivery_point_item">
                        г. Екатеринбург,  ул. Заводская, 17
                    </li>
                    <li class="delivery_point_item">
                        г. Екатеринбург, ул. Опалихинская, 15
                    </li>
                    <li class="delivery_point_item">
                        г. Екатеринбург, ул. Техническая, 63, ТЦ "7 ключей" (1 этаж)
                    </li>
                    <li class="delivery_point_item">
                        г. Екатеринбург, ул. 8 марта, 149, ТРЦ "Мегаполис" (4 этаж)
                    </li>
                    <li class="delivery_point_item">
                        г. Екатеринбург, ул. Вильгельма де Геннина, 33
                    </li>
                    <li class="delivery_point_item">
                        г. Екатеринбург, ул.Викулова, 33/1
                    </li>
                    <li class="delivery_point_item">
                        г.Екатеринбург,ул. Сыромолотова,22
                    </li>
                    <li class="delivery_point_item">
                        г. Верхняя Пышма, ул. Уральских рабочих, 49
                    </li>
                    <li class="delivery_point_item">
                        г. Березовский, ул. Красных героев, 3
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript"><!--
	function updateCart(key, quantity) {
	 if (quantity != 0 && quantity != 'undefined') {
	  $.ajax({
	   type: 'post',
	   data: 'quantity['+key+']='+quantity,
	   url: 'index.php?route=checkout/cart',
	   dataType: 'html',
	   success: function(data) {
		$('.cart-info').load('index.php?route=module/cart .cart-info');
	   }
	  });
	 }
	}
    $("#phone").mask("+7 999 999-99-99");
	$('.error_submit').hide();
	$('#submit').click(function(){
		
		var error = false;
		
		if ($('#name').val()) {
			$('#error_name').hide();
		} else {
			error=true;
			$('#error_name').show();
		}
		
		if ($('#surname').val()) {
			$('#error_surname').hide();
		} else {
			error=true;
			$('#error_surname').show();
		}
		
		if ($('#phone').val()) {
			$('#error_phone').hide();
		} else {
			error=true;
			$('#error_phone').show();
		}
		
		if ($('#email').val()) {
			$('#error_email').hide();
		} else {
			error=true;
			$('#error_email').show();
		}
		
		switch ($('input[name=select_delivery]:checked').attr('id')) {
			case 'select_courier':
				var address='';
				
				if($('#street').val())
					address+='ул. '+$('#street').val();
				
				if($('#house').val())
					address+=' д. '+$('#house').val();
				
				if($('#building').val())
					address+=' корп. '+$('#building').val();
				
				if($('#apartment').val())
					address+=' кв. '+$('#apartment').val();
				
				var delivery=1;
				$('#error_delivery').hide();
			break;
			
			case 'select_pickup':
				var delivery=2;
				$('#error_delivery').hide();
				var address=$('#delivery_point').text();
			break;
			
			default:
				error=true;
				$('#error_delivery').show();
			break;
		}
		
		switch ($('input[name=select_payment]:checked').attr('id')) {
			case 'payment_cash':
				var payment=1;
				$('#error_payment').hide();
			break;
			
			case 'payment_card':
				var payment=2;
				$('#error_payment').hide();
			break;
			
			case 'payment_electronic':
				var payment=3;
				$('#error_payment').hide();
			break;
			
			default:
				error=true;
				$('#error_payment').show();
			break;
		}
		
		if (!error) {
			$.redirect('index.php?route=checkout/confirm', 
				{
					'name': $('#name').val(), 
					'surname': $('#surname').val(),
					'phone': $('#phone').val(), 
					'email': $('#email').val(),
					'delivery': delivery,
					'payment': payment,
					'country_id': 176,
					'zone_id': 126,
					'address': address
				}
			);
		}
	});
//--></script>
<?php echo $footer; ?>