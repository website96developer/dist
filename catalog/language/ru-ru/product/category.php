<?php
// Text
$_['text_refine']       = 'Выберите подкатегорию';
$_['text_product']      = 'Товары';
$_['text_error']        = 'Категория не найдена!';
$_['text_empty']        = 'В данной категории нет товаров.';
$_['text_quantity']     = 'Количество:';
$_['text_manufacturer'] = 'Производитель:';
$_['text_model']        = 'Модель:';
$_['text_points']       = 'Бонусные баллы:';
$_['text_price']        = 'Цена:';
$_['text_tax']          = 'Без НДС:';
$_['text_compare']      = 'Сравнение товаров (%s)';
$_['text_sort']         = 'Сортировка:';
$_['text_default']      = 'По умолчанию';
$_['text_added']        = 'По новизне';
$_['text_status']       = 'По статусу';
$_['text_name_asc']     = 'Название (А - Я)';
$_['text_name_desc']    = 'Название (Я - А)';
$_['text_price_asc']    = 'По стоимости';
$_['text_price_desc']   = 'По стоимости';
$_['text_rating_asc']   = 'Рейтинг (начиная с низкого)';
$_['text_rating_desc']  = 'Рейтинг (начиная с высокого)';
$_['text_model_asc']    = 'Модель (А - Я)';
$_['text_model_desc']   = 'Модель (Я - А)';
$_['text_limit']        = 'Показать:';

