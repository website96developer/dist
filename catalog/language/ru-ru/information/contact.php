<?php
// Heading
$_['heading_title']  = 'Поддержка клиентов';

// Text
$_['text_location']  = 'Наше местонахождение';
$_['text_store']     = 'Наши магазины';
$_['text_contact']   = 'Форма обратной связи';
$_['text_address']   = 'Адрес';
$_['text_telephone'] = 'Телефон';
$_['text_fax']       = 'Факс';
$_['text_open']      = 'Режим работы';
$_['text_comment']   = 'Дополнительная информация';
$_['text_success']   = '<p>Ваше сообщение успешно отправлено владельцу магазина!</p>';
$_['button_submit']  = 'Отправить сообщение';

// Entry
$_['entry_name']     = 'Ваше имя';
$_['entry_email']    = 'Ваш E-Mail';
$_['entry_enquiry']  = 'Ваш вопрос или сообщение';
$_['entry_captcha']  = 'Введите код, указанный на картинке';

// Email
$_['email_subject']  = 'Сообщение от %s';



// Errors
$_['error_name']     = 'Введите имя';
$_['error_surname']  = 'Введите фамилию';
$_['error_email']    = 'Введите email';
$_['error_enquiry']  = 'Введите текст сообщения';
$_['error_captcha']  = 'Проверочный код не совпадает с изображением!';
$_['error_telefon'] = 'Введите телефон';

