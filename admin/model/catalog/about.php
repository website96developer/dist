<?php
class ModelCatalogAbout extends Model {

	public function addAbout($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "about SET status = '" . (int)$data['status'] . "', date_added = now()");
	
		$about_id = $this->db->getLastId();
	
		if (isset($data['image'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "about SET image = '" . $this->db->escape($data['image']) . "' WHERE about_id = '" . (int)$about_id . "'");
		}
	
		if (isset($data['date_added'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "about SET date_added = '" . $this->db->escape($data['date_added']) . "' WHERE about_id = '" . (int)$about_id . "'");
		}
	
		foreach ($data['about_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "about_description SET about_id = '" . (int)$about_id . "', language_id = '" . (int)$language_id . "', title = '" . $this->db->escape($value['title']) . "',  description = '" . $this->db->escape($value['description']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "',  meta_h1 = '" . $this->db->escape($value['meta_h1']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
		}
	
		if (isset($data['about_store'])) {
			foreach ($data['about_store'] as $store_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "about_to_store SET about_id = '" . (int)$about_id . "', store_id = '" . (int)$store_id . "'");
			}
		}
		
		if ($data['keyword']) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'about_id=" . (int)$about_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
		}
        
        if (isset($data['about_image'])) {
			foreach ($data['about_image'] as $about_image) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "about_image SET about_id = '" . (int)$about_id . "', image = '" . $this->db->escape($about_image['image']) . "', sort_order = '" . (int)$about_image['sort_order'] . "'");
			}
		}
	
		$this->cache->delete('about');
	}

	public function editAbout($about_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "about SET status = '" . (int)$data['status'] . "' WHERE about_id = '" . (int)$about_id . "'");

		if (isset($data['image'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "about SET image = '" . $this->db->escape($data['image']) . "' WHERE about_id = '" . (int)$about_id . "'");
		}
		
		if (isset($data['date_added'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "about SET date_added = '" . $this->db->escape($data['date_added']) . "' WHERE about_id = '" . (int)$about_id . "'");
		}
		
		$this->db->query("DELETE FROM " . DB_PREFIX . "about_description WHERE about_id = '" . (int)$about_id . "'");
	
		foreach ($data['about_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "about_description SET about_id = '" . (int)$about_id . "', language_id = '" . (int)$language_id . "', title = '" . $this->db->escape($value['title']) . "', description = '" . $this->db->escape($value['description']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "',  meta_h1 = '" . $this->db->escape($value['meta_h1']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
		}
	
		$this->db->query("DELETE FROM " . DB_PREFIX . "about_to_store WHERE about_id = '" . (int)$about_id . "'");
	
		if (isset($data['about_store'])) {		
			foreach ($data['about_store'] as $store_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "about_to_store SET about_id = '" . (int)$about_id . "', store_id = '" . (int)$store_id . "'");
			}
		}
        
        $this->db->query("DELETE FROM " . DB_PREFIX . "about_image WHERE about_id = '" . (int)$about_id . "'");

		if (isset($data['about_image'])) {
			foreach ($data['about_image'] as $about_image) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "about_image SET about_id = '" . (int)$about_id . "', image = '" . $this->db->escape($about_image['image']) . "', sort_order = '" . (int)$about_image['sort_order'] . "'");
			}
		}
	
		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'about_id=" . (int)$about_id . "'");
	
		if ($data['keyword']) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'about_id=" . (int)$about_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
		}
	
		$this->cache->delete('about');
	}

	public function deleteAbout($about_id) { 
		$this->db->query("DELETE FROM " . DB_PREFIX . "about WHERE about_id = '" . (int)$about_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "about_description WHERE about_id = '" . (int)$about_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "about_to_store WHERE about_id = '" . (int)$about_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'about_id=" . (int)$about_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "about_image WHERE about_id = '" . (int)$about_id . "'");
	
		$this->cache->delete('about');
	}

	public function getAboutList($data = array()) {
		if ($data) {
			$sql = "SELECT * FROM " . DB_PREFIX . "about n LEFT JOIN " . DB_PREFIX . "about_description nd ON (n.about_id = nd.about_id) WHERE nd.language_id = '" . (int)$this->config->get('config_language_id') . "'";

			$sort_data = array(
				'nd.title',
				'n.date_added'
			);

			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY " . $data['sort'];
			} else {
				$sql .= " ORDER BY nd.title";
			}

			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}

			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}

				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}

			$query = $this->db->query($sql);

			return $query->rows;
		} else {
			$about_data = $this->cache->get('about.' . (int)$this->config->get('config_language_id'));

			if (!$about_data) {
				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "about n LEFT JOIN " . DB_PREFIX . "about_description nd ON (n.about_id = nd.about_id) WHERE nd.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY nd.title");

				$about_data = $query->rows;

				$this->cache->set('about.' . (int)$this->config->get('config_language_id'), $about_data);
			}

			return $about_data;
		}
	}
    
    public function getAboutImages($about_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "about_image WHERE about_id = '" . (int)$about_id . "' ORDER BY sort_order ASC");

		return $query->rows;
	}

	public function getaboutStory($about_id) { 
		$query = $this->db->query("SELECT DISTINCT *, (SELECT keyword FROM " . DB_PREFIX . "url_alias WHERE query = 'about_id=" . (int)$about_id . "') AS keyword FROM " . DB_PREFIX . "about n LEFT JOIN " . DB_PREFIX . "about_description nd ON (n.about_id = nd.about_id) WHERE n.about_id = '" . (int)$about_id . "' AND nd.language_id = '" . (int)$this->config->get('config_language_id') . "'");
	
		return $query->row;
	}

	public function getAboutDescriptions($about_id) { 
		$about_description_data = array();
	
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "about_description WHERE about_id = '" . (int)$about_id . "'");
	
		foreach ($query->rows as $result) {
			$about_description_data[$result['language_id']] = array(
				'title'            	=> $result['title'],
				'description'      	=> $result['description'],
				'meta_title'        => $result['meta_title'],
				'meta_h1'           => $result['meta_h1'],
				'meta_description' 	=> $result['meta_description'],
				'meta_keyword'		=> $result['meta_keyword'],
			);
		}
	
		return $about_description_data;
	}

	public function getAboutStores($about_id) { 
		$aboutpage_store_data = array();
	
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "about_to_store WHERE about_id = '" . (int)$about_id . "'");
		
		foreach ($query->rows as $result) {
			$aboutpage_store_data[] = $result['store_id'];
		}
	
		return $aboutpage_store_data;
	}

	public function getTotalAbout() { 

     	$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "about");
	
		return $query->row['total'];
	}

	public function setAboutListUrl($url) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE query = 'information/about'");
		if ($query) {
			$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'information/about'");
			$this->db->query("INSERT INTO `" . DB_PREFIX . "url_alias` SET `query` = 'information/about', `keyword` = '" . $this->db->escape($url) . "'");
		}else{
			$this->db->query("INSERT INTO `" . DB_PREFIX . "url_alias` SET `query` = 'information/about', `keyword` = '" . $this->db->escape($url) . "'");
		}
	}

	public function getAboutListUrl($query) {
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE query = '" . $this->db->escape($query) . "'");
			if($query->rows){
				return $query->row['keyword'];
			}else{
				return false;
			}
	}
}
?>