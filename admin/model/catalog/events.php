<?php
class ModelCatalogEvents extends Model {

	public function addEvents($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "events SET status = '" . (int)$data['status'] . "', date_added = now(), date = now()");
	
		$events_id = $this->db->getLastId();
	
		if (isset($data['image'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "events SET image = '" . $this->db->escape($data['image']) . "' WHERE events_id = '" . (int)$events_id . "'");
		}
	
		if (isset($data['date_added'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "events SET date_added = '" . $this->db->escape($data['date_added']) . "' WHERE events_id = '" . (int)$events_id . "'");
		}
        
        if (isset($data['date'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "events SET date = '" . $this->db->escape($data['date']) . "' WHERE events_id = '" . (int)$events_id . "'");
		}
	
		foreach ($data['events_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "events_description SET events_id = '" . (int)$events_id . "', language_id = '" . (int)$language_id . "', title = '" . $this->db->escape($value['title']) . "',  description = '" . $this->db->escape($value['description']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "',  meta_h1 = '" . $this->db->escape($value['meta_h1']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
		}
	
		if (isset($data['events_store'])) {
			foreach ($data['events_store'] as $store_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "events_to_store SET events_id = '" . (int)$events_id . "', store_id = '" . (int)$store_id . "'");
			}
		}
		
		if ($data['keyword']) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'events_id=" . (int)$events_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
		}
	
		$this->cache->delete('events');
	}

	public function editEvents($events_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "events SET status = '" . (int)$data['status'] . "' WHERE events_id = '" . (int)$events_id . "'");

		if (isset($data['image'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "events SET image = '" . $this->db->escape($data['image']) . "' WHERE events_id = '" . (int)$events_id . "'");
		}
		
		if (isset($data['date_added'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "events SET date_added = '" . $this->db->escape($data['date_added']) . "' WHERE events_id = '" . (int)$events_id . "'");
		}
        
        if (isset($data['category'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "events SET category = '" . $data['category'] . "' WHERE events_id = '" . (int)$events_id . "'");
		}
        
        if (isset($data['date'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "events SET date = '" . $this->db->escape($data['date']) . "' WHERE events_id = '" . (int)$events_id . "'");
		}
		
		$this->db->query("DELETE FROM " . DB_PREFIX . "events_description WHERE events_id = '" . (int)$events_id . "'");
	
		foreach ($data['events_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "events_description SET events_id = '" . (int)$events_id . "', language_id = '" . (int)$language_id . "', title = '" . $this->db->escape($value['title']) . "', description = '" . $this->db->escape($value['description']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "',  meta_h1 = '" . $this->db->escape($value['meta_h1']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
		}
	
		$this->db->query("DELETE FROM " . DB_PREFIX . "events_to_store WHERE events_id = '" . (int)$events_id . "'");
	
		if (isset($data['events_store'])) {		
			foreach ($data['events_store'] as $store_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "events_to_store SET events_id = '" . (int)$events_id . "', store_id = '" . (int)$store_id . "'");
			}
		}
	
		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'events_id=" . (int)$events_id . "'");
	
		if ($data['keyword']) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'events_id=" . (int)$events_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
		}
	
		$this->cache->delete('events');
	}

	public function deleteEvents($events_id) { 
		$this->db->query("DELETE FROM " . DB_PREFIX . "events WHERE events_id = '" . (int)$events_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "events_description WHERE events_id = '" . (int)$events_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "events_to_store WHERE events_id = '" . (int)$events_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'events_id=" . (int)$events_id . "'");
	
		$this->cache->delete('events');
	}

	public function getEventsList($data = array()) {
		if ($data) {
			$sql = "SELECT * FROM " . DB_PREFIX . "events n LEFT JOIN " . DB_PREFIX . "events_description nd ON (n.events_id = nd.events_id) WHERE nd.language_id = '" . (int)$this->config->get('config_language_id') . "'";

			$sort_data = array(
				'nd.title',
				'n.date_added'
			);

			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY " . $data['sort'];
			} else {
				$sql .= " ORDER BY nd.title";
			}

			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}

			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}

				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}

			$query = $this->db->query($sql);

			return $query->rows;
		} else {
			$events_data = $this->cache->get('events.' . (int)$this->config->get('config_language_id'));

			if (!$events_data) {
				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "events n LEFT JOIN " . DB_PREFIX . "events_description nd ON (n.events_id = nd.events_id) WHERE nd.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY nd.title");

				$events_data = $query->rows;

				$this->cache->set('events.' . (int)$this->config->get('config_language_id'), $events_data);
			}

			return $events_data;
		}
	}

	public function getEventsStory($events_id) { 
		$query = $this->db->query("SELECT DISTINCT *, (SELECT keyword FROM " . DB_PREFIX . "url_alias WHERE query = 'events_id=" . (int)$events_id . "') AS keyword FROM " . DB_PREFIX . "events n LEFT JOIN " . DB_PREFIX . "events_description nd ON (n.events_id = nd.events_id) WHERE n.events_id = '" . (int)$events_id . "' AND nd.language_id = '" . (int)$this->config->get('config_language_id') . "'");
	
		return $query->row;
	}

	public function getEventsDescriptions($events_id) { 
		$events_description_data = array();
	
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "events_description WHERE events_id = '" . (int)$events_id . "'");
	
		foreach ($query->rows as $result) {
			$events_description_data[$result['language_id']] = array(
				'title'            	=> $result['title'],
				'description'      	=> $result['description'],
				'meta_title'        => $result['meta_title'],
				'meta_h1'           => $result['meta_h1'],
				'meta_description' 	=> $result['meta_description'],
				'meta_keyword'		=> $result['meta_keyword'],
			);
		}
	
		return $events_description_data;
	}

	public function getEventsStores($events_id) { 
		$eventspage_store_data = array();
	
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "events_to_store WHERE events_id = '" . (int)$events_id . "'");
		
		foreach ($query->rows as $result) {
			$eventspage_store_data[] = $result['store_id'];
		}
	
		return $eventspage_store_data;
	}

	public function getTotalEvents() { 

     	$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "events");
	
		return $query->row['total'];
	}

	public function setEventsListUrl($url) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE query = 'information/events'");
		if ($query) {
			$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'information/events'");
			$this->db->query("INSERT INTO `" . DB_PREFIX . "url_alias` SET `query` = 'information/events', `keyword` = '" . $this->db->escape($url) . "'");
		}else{
			$this->db->query("INSERT INTO `" . DB_PREFIX . "url_alias` SET `query` = 'information/events', `keyword` = '" . $this->db->escape($url) . "'");
		}
	}

	public function getEventsListUrl($query) {
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE query = '" . $this->db->escape($query) . "'");
			if($query->rows){
				return $query->row['keyword'];
			}else{
				return false;
			}
	}
}
?>