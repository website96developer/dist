<?php
class ModelCatalogServices extends Model {

	public function addServices($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "services SET status = '" . (int)$data['status'] . "', date_added = now()");
	
		$services_id = $this->db->getLastId();
	
		if (isset($data['image'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "services SET image = '" . $this->db->escape($data['image']) . "' WHERE services_id = '" . (int)$services_id . "'");
		}
	
		if (isset($data['date_added'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "services SET date_added = '" . $this->db->escape($data['date_added']) . "' WHERE services_id = '" . (int)$services_id . "'");
		}
	
		foreach ($data['services_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "services_description SET services_id = '" . (int)$services_id . "', language_id = '" . (int)$language_id . "', title = '" . $this->db->escape($value['title']) . "',  description = '" . $this->db->escape($value['description']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "',  meta_h1 = '" . $this->db->escape($value['meta_h1']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
		}
	
		if (isset($data['services_store'])) {
			foreach ($data['services_store'] as $store_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "services_to_store SET services_id = '" . (int)$services_id . "', store_id = '" . (int)$store_id . "'");
			}
		}
        
        if (isset($data['services_image'])) {
			foreach ($data['services_image'] as $services_image) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "services_image SET services_id = '" . (int)$services_id . "', image = '" . $this->db->escape($services_image['image']) . "', sort_order = '" . (int)$services_image['sort_order'] . "'");
			}
		}
		
		if ($data['keyword']) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'services_id=" . (int)$services_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
		}
	
		$this->cache->delete('services');
	}

	public function editServices($services_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "services SET status = '" . (int)$data['status'] . "' WHERE services_id = '" . (int)$services_id . "'");

		if (isset($data['image'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "services SET image = '" . $this->db->escape($data['image']) . "' WHERE services_id = '" . (int)$services_id . "'");
		}
		
		if (isset($data['date_added'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "services SET date_added = '" . $this->db->escape($data['date_added']) . "' WHERE services_id = '" . (int)$services_id . "'");
		}
		
		$this->db->query("DELETE FROM " . DB_PREFIX . "services_description WHERE services_id = '" . (int)$services_id . "'");
	
		foreach ($data['services_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "services_description SET services_id = '" . (int)$services_id . "', language_id = '" . (int)$language_id . "', title = '" . $this->db->escape($value['title']) . "', description = '" . $this->db->escape($value['description']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "',  meta_h1 = '" . $this->db->escape($value['meta_h1']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
		}
	
		$this->db->query("DELETE FROM " . DB_PREFIX . "services_to_store WHERE services_id = '" . (int)$services_id . "'");
	
		if (isset($data['services_store'])) {		
			foreach ($data['services_store'] as $store_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "services_to_store SET services_id = '" . (int)$services_id . "', store_id = '" . (int)$store_id . "'");
			}
		}
        
        $this->db->query("DELETE FROM " . DB_PREFIX . "services_image WHERE services_id = '" . (int)$services_id . "'");
        
        if (isset($data['services_image'])) {
			foreach ($data['services_image'] as $services_image) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "services_image SET services_id = '" . (int)$services_id . "', image = '" . $this->db->escape($services_image['image']) . "', sort_order = '" . (int)$services_image['sort_order'] . "'");
			}
		}
	
		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'services_id=" . (int)$services_id . "'");
	
		if ($data['keyword']) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'services_id=" . (int)$services_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
		}
	
		$this->cache->delete('services');
	}

	public function deleteServices($services_id) { 
		$this->db->query("DELETE FROM " . DB_PREFIX . "services WHERE services_id = '" . (int)$services_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "services_description WHERE services_id = '" . (int)$services_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "services_to_store WHERE services_id = '" . (int)$services_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'services_id=" . (int)$services_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "services_image WHERE services_id = '" . (int)$services_id . "'");
	
		$this->cache->delete('services');
	}

	public function getServicesList($data = array()) {
		if ($data) {
			$sql = "SELECT * FROM " . DB_PREFIX . "services n LEFT JOIN " . DB_PREFIX . "services_description nd ON (n.services_id = nd.services_id) WHERE nd.language_id = '" . (int)$this->config->get('config_language_id') . "'";

			$sort_data = array(
				'nd.title',
				'n.date_added'
			);

			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY " . $data['sort'];
			} else {
				$sql .= " ORDER BY nd.title";
			}

			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}

			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}

				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}

			$query = $this->db->query($sql);

			return $query->rows;
		} else {
			$services_data = $this->cache->get('services.' . (int)$this->config->get('config_language_id'));

			if (!$services_data) {
				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "services n LEFT JOIN " . DB_PREFIX . "services_description nd ON (n.services_id = nd.services_id) WHERE nd.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY nd.title");

				$services_data = $query->rows;

				$this->cache->set('services.' . (int)$this->config->get('config_language_id'), $services_data);
			}

			return $services_data;
		}
	}
    
    public function getServicesImages($services_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "services_image WHERE services_id = '" . (int)$services_id . "' ORDER BY sort_order ASC");

		return $query->rows;
	}

	public function getServicesStory($services_id) { 
		$query = $this->db->query("SELECT DISTINCT *, (SELECT keyword FROM " . DB_PREFIX . "url_alias WHERE query = 'services_id=" . (int)$services_id . "') AS keyword FROM " . DB_PREFIX . "services n LEFT JOIN " . DB_PREFIX . "services_description nd ON (n.services_id = nd.services_id) WHERE n.services_id = '" . (int)$services_id . "' AND nd.language_id = '" . (int)$this->config->get('config_language_id') . "'");
	
		return $query->row;
	}

	public function getServicesDescriptions($services_id) { 
		$services_description_data = array();
	
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "services_description WHERE services_id = '" . (int)$services_id . "'");
	
		foreach ($query->rows as $result) {
			$services_description_data[$result['language_id']] = array(
				'title'            	=> $result['title'],
				'description'      	=> $result['description'],
				'meta_title'        => $result['meta_title'],
				'meta_h1'           => $result['meta_h1'],
				'meta_description' 	=> $result['meta_description'],
				'meta_keyword'		=> $result['meta_keyword'],
			);
		}
	
		return $services_description_data;
	}

	public function getServicesStores($services_id) { 
		$servicespage_store_data = array();
	
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "services_to_store WHERE services_id = '" . (int)$services_id . "'");
		
		foreach ($query->rows as $result) {
			$servicespage_store_data[] = $result['store_id'];
		}
	
		return $servicespage_store_data;
	}

	public function getTotalServices() { 

     	$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "services");
	
		return $query->row['total'];
	}

	public function setservicesListUrl($url) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE query = 'information/services'");
		if ($query) {
			$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'information/services'");
			$this->db->query("INSERT INTO `" . DB_PREFIX . "url_alias` SET `query` = 'information/services', `keyword` = '" . $this->db->escape($url) . "'");
		}else{
			$this->db->query("INSERT INTO `" . DB_PREFIX . "url_alias` SET `query` = 'information/services', `keyword` = '" . $this->db->escape($url) . "'");
		}
	}

	public function getServicesListUrl($query) {
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE query = '" . $this->db->escape($query) . "'");
			if($query->rows){
				return $query->row['keyword'];
			}else{
				return false;
			}
	}
}
?>